
if { ! [file isdirectory $compile_directory ] } {
	file mkdir $compile_directory
}

if { [ file exists $proj.xise ] } { 
        project open $proj.xise
} else { 
	project new $proj.xise
	project set family Spartan6
	project set device xc6slx16
	project set package  ftg256
	project set speed -3
	project set "Working Directory" $compile_directory
	project set "Preferred Language" VHDL

	foreach filename $hdl_files { 
		xfile add $filename
		puts "Adding file $filename to the project."
	}

	project close
	project open $proj.xise
}

process run "Implement Design"

process run "Generate Programming File"

set impact_script_filename "impact_script.txt"
set bit_filename output_dir/$proj.bit

if { [ catch { set f_id [ open $impact_script_filename w ] } msg ] } { 
	puts "Can't create $impact_script_filename"
	puts $msg
	exit
}

puts $f_id "setMode -bscan\nsetCable -p auto\n"
puts $f_id "addDevice -position 1 -file $bit_filename\n"
puts $f_id "program -p 1\nquit\n"

close $f_id

set ::env(LD_PRELOAD) /opt/Xilinx/usb-driver/libusb-driver.so

set impact_p [ open "| impact -batch $impact_script_filename" r ]

while { ! [ eof $impact_p ] } { 
	gets $impact_p line
	puts $line 
}

