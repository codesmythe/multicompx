#!/usr/bin/env xtclsh
set proj Z80Multicomp
set compile_directory output_dir
set top_name Microcomputer
set bit_filename $compile_directory/$top_name.bit
set constraints_file Spartan6_Board.ucf 
set hdl_files [ list \
   Microcomputer.vhd \
   $constraints_file \
   ../../src/Components/Z80/T80_ALU.vhd \
   ../../src/Components/Z80/T80_MCode.vhd \
   ../../src/Components/Z80/T80_Pack.vhd \
   ../../src/Components/Z80/T80.vhd \
   ../../src/Components/Z80/T80s.vhd \
   ../../src/Components/Z80/T80_Reg.vhd \
   ../../src/Components/MMU/MMU4.vhd \
   ../../src/Components/UART/bufferedUART.vhd \
   ../../src/Components/BRG/brg.vhd \
   ../../src/Components/SDCARD/sd_controller.vhd \
   ../../src/Components/TERMINAL/SBCTextDisplayRGB.vhd \
   ../../src/Components/TERMINAL/CGABoldRom.vhd \
   ../../src/Components/TERMINAL/CGABoldRomReduced.vhd \
   ../../src/Components/TERMINAL/DisplayRam1K.vhd \
   ../../src/Components/TERMINAL/DisplayRam2K.vhd \
   ../../src/Components/TERMINAL/keyMapRom.vhd \
   ../../src/ROMS/Z80/Z80_CMON_ROM.vhd \
]

source ../build_master.tcl
