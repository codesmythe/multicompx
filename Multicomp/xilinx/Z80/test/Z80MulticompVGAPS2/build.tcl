#!/usr/bin/env xtclsh
set proj Z80MulticompVGAPS2
set compile_directory output_dir
set top_name Microcomputer
set bit_filename $compile_directory/$top_name.bit
set constraints_file Spartan6_Board.ucf 
set hdl_files [ list \
  Microcomputer.vhd \
  $constraints_file \
  ../../../../src/Components/Z80/T80s.vhd \
  ../../../../src/Components/Z80/T80_ALU.vhd \
  ../../../../src/Components/Z80/T80_MCode.vhd \
  ../../../../src/Components/Z80/T80_Pack.vhd \
  ../../../../src/Components/Z80/T80_RegX.vhd \
  ../../../../src/Components/Z80/T80.vhd \
  ../../../../src/ROMS/Z80/Z80_BASIC_ROM.vhd \
  ../../../../src/Components/UART/bufferedUART.vhd \
  ../../../../src/Components/TERMINAL/SBCTextDisplayRGB.vhd \
  ../../../../src/Components/TERMINAL/CGABoldRom.vhd \
  ../../../../src/Components/TERMINAL/CGABoldRomReduced.vhd \
  ../../../../src/Components/TERMINAL/DisplayRam1K.vhd \
  ../../../../src/Components/TERMINAL/DisplayRam2K.vhd \
  ../../../../src/Components/TERMINAL/keyMapRom.vhd \
]

source ../../../build_master.tcl
