.. MulticompX documentation master file, created by
   sphinx-quickstart on Sat Nov 21 12:23:20 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MulticompX's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   doc/description.rst
   doc/assembly_guide.rst
   doc/BOM.rst
   doc/PCB.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
