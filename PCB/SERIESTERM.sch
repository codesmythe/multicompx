EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 5 7
Title "MulticompX System Board"
Date "2020-11-29"
Rev "1.0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 "From an original design by David G. Conroy (http://www.fpgaretrocomputing.org)"
Comment4 "Copyright © 2020 Rob Gowin. Licensed under CC BY-NC-SA 4.0."
$EndDescr
Wire Wire Line
	5050 1900 4750 1900
Wire Wire Line
	5450 1900 5750 1900
Entry Wire Line
	5750 1900 5850 2000
Entry Wire Line
	4650 2000 4750 1900
Wire Wire Line
	5050 2000 4750 2000
Wire Wire Line
	5450 2000 5750 2000
Entry Wire Line
	5750 2000 5850 2100
Entry Wire Line
	4650 2100 4750 2000
Wire Wire Line
	5050 2100 4750 2100
Wire Wire Line
	5450 2100 5750 2100
Entry Wire Line
	5750 2100 5850 2200
Entry Wire Line
	4650 2200 4750 2100
Wire Wire Line
	5050 2200 4750 2200
Wire Wire Line
	5450 2200 5750 2200
Entry Wire Line
	5750 2200 5850 2300
Entry Wire Line
	4650 2300 4750 2200
Wire Wire Line
	5050 2300 4750 2300
Wire Wire Line
	5450 2300 5750 2300
Entry Wire Line
	5750 2300 5850 2400
Entry Wire Line
	4650 2400 4750 2300
Wire Wire Line
	5050 2400 4750 2400
Wire Wire Line
	5450 2400 5750 2400
Entry Wire Line
	5750 2400 5850 2500
Entry Wire Line
	4650 2500 4750 2400
Wire Wire Line
	5050 2500 4750 2500
Wire Wire Line
	5450 2500 5750 2500
Entry Wire Line
	5750 2500 5850 2600
Entry Wire Line
	4650 2600 4750 2500
Wire Wire Line
	5050 2600 4750 2600
Wire Wire Line
	5450 2600 5750 2600
Entry Wire Line
	5750 2600 5850 2700
Entry Wire Line
	4650 2700 4750 2600
Wire Wire Line
	5050 3050 4750 3050
Wire Wire Line
	5450 3050 5750 3050
Entry Wire Line
	5750 3050 5850 3150
Entry Wire Line
	4650 3150 4750 3050
Wire Wire Line
	5050 3150 4750 3150
Wire Wire Line
	5450 3150 5750 3150
Entry Wire Line
	5750 3150 5850 3250
Entry Wire Line
	4650 3250 4750 3150
Wire Wire Line
	5050 3250 4750 3250
Wire Wire Line
	5450 3250 5750 3250
Entry Wire Line
	5750 3250 5850 3350
Entry Wire Line
	4650 3350 4750 3250
Wire Wire Line
	5050 3350 4750 3350
Wire Wire Line
	5450 3350 5750 3350
Entry Wire Line
	5750 3350 5850 3450
Entry Wire Line
	4650 3450 4750 3350
Wire Wire Line
	5050 3450 4750 3450
Wire Wire Line
	5450 3450 5750 3450
Entry Wire Line
	5750 3450 5850 3550
Entry Wire Line
	4650 3550 4750 3450
Wire Wire Line
	5050 3550 4750 3550
Wire Wire Line
	5450 3550 5750 3550
Entry Wire Line
	5750 3550 5850 3650
Entry Wire Line
	4650 3650 4750 3550
Wire Wire Line
	5050 3650 4750 3650
Wire Wire Line
	5450 3650 5750 3650
Entry Wire Line
	5750 3650 5850 3750
Entry Wire Line
	4650 3750 4750 3650
Wire Wire Line
	5050 3750 4750 3750
Wire Wire Line
	5450 3750 5750 3750
Entry Wire Line
	5750 3750 5850 3850
Entry Wire Line
	4650 3850 4750 3750
Wire Bus Line
	4050 1800 4650 1800
Wire Bus Line
	5850 1800 6400 1800
Text HLabel 4050 1800 0    50   Input ~ 0
MA[17..35]
Text HLabel 6400 1800 2    50   Output ~ 0
MAR[17..35]
Text Label 4800 1900 0    50   ~ 0
MA35
Text Label 4800 2000 0    50   ~ 0
MA34
Text Label 4800 2100 0    50   ~ 0
MA33
Text Label 4800 2200 0    50   ~ 0
MA32
Text Label 5500 1900 0    50   ~ 0
MAR35
Text Label 5500 2000 0    50   ~ 0
MAR34
Text Label 5500 2100 0    50   ~ 0
MAR33
Text Label 5500 2200 0    50   ~ 0
MAR32
Text Label 5500 2300 0    50   ~ 0
MAR31
Text Label 5500 2400 0    50   ~ 0
MAR30
Text Label 5500 2500 0    50   ~ 0
MAR29
Text Label 5500 2600 0    50   ~ 0
MAR28
Text Label 4800 2300 0    50   ~ 0
MA31
Text Label 4800 2400 0    50   ~ 0
MA30
Text Label 4800 2500 0    50   ~ 0
MA29
Text Label 4800 2600 0    50   ~ 0
MA28
Text Label 4800 3050 0    50   ~ 0
MA27
Text Label 4800 3150 0    50   ~ 0
MA26
Text Label 4800 3250 0    50   ~ 0
MA25
Text Label 4800 3350 0    50   ~ 0
MA24
Text Label 4800 3450 0    50   ~ 0
MA23
Text Label 4800 3550 0    50   ~ 0
MA22
Text Label 4800 3650 0    50   ~ 0
MA21
Text Label 4800 3750 0    50   ~ 0
MA20
Text Label 5500 3050 0    50   ~ 0
MAR27
Text Label 5500 3150 0    50   ~ 0
MAR26
Text Label 5500 3250 0    50   ~ 0
MAR25
Text Label 5500 3350 0    50   ~ 0
MAR24
Text Label 5500 3450 0    50   ~ 0
MAR23
Text Label 5500 3550 0    50   ~ 0
MAR22
Text Label 5500 3650 0    50   ~ 0
MAR21
Text Label 5500 3750 0    50   ~ 0
MAR20
$Comp
L Device:R_Pack08 RN401
U 1 1 5EC1087E
P 5250 2300
F 0 "RN401" V 4633 2300 50  0000 C CNN
F 1 "R_Pack08 33ohms" V 4724 2300 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket_LongPads" V 5725 2300 50  0001 C CNN
F 3 "~" H 5250 2300 50  0001 C CNN
	1    5250 2300
	0    1    1    0   
$EndComp
$Comp
L Device:R_Pack08 RN402
U 1 1 5EC1E963
P 5250 3450
F 0 "RN402" V 4633 3450 50  0000 C CNN
F 1 "R_Pack08 33ohms" V 4724 3450 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket_LongPads" V 5725 3450 50  0001 C CNN
F 3 "~" H 5250 3450 50  0001 C CNN
	1    5250 3450
	0    1    1    0   
$EndComp
Wire Wire Line
	5450 4600 6000 4600
Wire Wire Line
	5450 4500 5800 4500
Wire Wire Line
	5900 4500 6000 4500
Wire Wire Line
	5900 4550 5900 4500
Wire Wire Line
	5800 4550 5900 4550
Wire Wire Line
	5800 4500 5800 4550
Wire Wire Line
	4700 4500 5050 4500
Wire Wire Line
	4700 4550 4700 4500
Wire Wire Line
	4600 4550 4700 4550
Wire Wire Line
	4500 4700 5050 4700
Wire Wire Line
	4500 4600 5050 4600
Wire Wire Line
	4600 4500 4500 4500
Wire Wire Line
	4600 4550 4600 4500
$Comp
L Device:R_Pack08 RN403
U 1 1 5EC2D1B2
P 5250 4600
F 0 "RN403" V 4633 4600 50  0000 C CNN
F 1 "R_Pack08 33ohms" V 4724 4600 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket_LongPads" V 5725 4600 50  0001 C CNN
F 3 "~" H 5250 4600 50  0001 C CNN
	1    5250 4600
	0    1    1    0   
$EndComp
Text HLabel 6000 5100 2    50   Output ~ 0
~MWE1R
Text HLabel 6000 4900 2    50   Output ~ 0
~MOE1R
Text HLabel 6000 4800 2    50   Output ~ 0
~MCS1R
Text HLabel 4500 5100 0    50   Input ~ 0
~MWE1
Text HLabel 4500 4900 0    50   Input ~ 0
~MOE1
Text HLabel 4500 4800 0    50   Input ~ 0
~MCS1
Wire Wire Line
	5050 4900 4500 4900
Wire Wire Line
	5050 4800 4500 4800
Text Label 5500 4400 0    50   ~ 0
MAR17
Text Label 5500 4300 0    50   ~ 0
MAR18
Text Label 4800 4400 0    50   ~ 0
MA17
Text Label 4800 4300 0    50   ~ 0
MA18
Text Label 5500 4200 0    50   ~ 0
MAR19
Text Label 4800 4200 0    50   ~ 0
MA19
Text HLabel 6000 4700 2    50   Output ~ 0
~MWE0R
Text HLabel 6000 4600 2    50   Output ~ 0
~MOE0R
Text HLabel 6000 4500 2    50   Output ~ 0
~MCS0R
Text HLabel 4500 4700 0    50   Input ~ 0
~MWE0
Text HLabel 4500 4600 0    50   Input ~ 0
~MOE0
Text HLabel 4500 4500 0    50   Input ~ 0
~MCS0
Entry Wire Line
	4650 4500 4750 4400
Entry Wire Line
	5750 4400 5850 4500
Wire Wire Line
	5450 4400 5750 4400
Wire Wire Line
	5050 4400 4750 4400
Entry Wire Line
	4650 4400 4750 4300
Entry Wire Line
	5750 4300 5850 4400
Wire Wire Line
	5450 4300 5750 4300
Wire Wire Line
	5050 4300 4750 4300
Entry Wire Line
	4650 4300 4750 4200
Entry Wire Line
	5750 4200 5850 4300
Wire Wire Line
	5450 4200 5750 4200
Wire Wire Line
	5050 4200 4750 4200
Wire Wire Line
	5450 4700 6000 4700
Wire Wire Line
	5450 4800 6000 4800
Wire Wire Line
	5450 4900 6000 4900
$Comp
L Device:R R401
U 1 1 5ECA931A
P 5250 5100
F 0 "R401" V 5350 5100 50  0000 C CNN
F 1 "33" V 5250 5100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5180 5100 50  0001 C CNN
F 3 "~" H 5250 5100 50  0001 C CNN
	1    5250 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	4500 5100 5100 5100
Wire Wire Line
	5400 5100 6000 5100
Wire Bus Line
	4650 1800 4650 4500
Wire Bus Line
	5850 1800 5850 4500
$EndSCHEMATC
