EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 6 7
Title "MulticompX System Board"
Date "2020-11-29"
Rev "1.0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 "From an original design by David G. Conroy (http://www.fpgaretrocomputing.org)"
Comment4 "Copyright © 2020 Rob Gowin. Licensed under CC BY-NC-SA 4.0."
$EndDescr
Text Label 7950 3800 0    50   ~ 0
MD7
Wire Wire Line
	6500 3100 7050 3100
Wire Wire Line
	6500 3200 7050 3200
Wire Wire Line
	6500 3300 7050 3300
Wire Wire Line
	6500 3500 7050 3500
Wire Wire Line
	6500 3600 7050 3600
Wire Wire Line
	6500 3700 7050 3700
Wire Wire Line
	6500 3800 7050 3800
Wire Wire Line
	6500 3900 7050 3900
Wire Wire Line
	6500 4000 7050 4000
Text Label 7950 4000 0    50   ~ 0
MD9
Text Label 7950 3900 0    50   ~ 0
MD8
Text Label 7950 4100 0    50   ~ 0
MD10
Text Label 7950 4200 0    50   ~ 0
MD11
Text Label 7950 4400 0    50   ~ 0
MD13
Text Label 7950 4600 0    50   ~ 0
MD15
Text Label 7950 4800 0    50   ~ 0
MD17
Text Label 7950 4300 0    50   ~ 0
MD12
Text Label 7950 4500 0    50   ~ 0
MD14
Text Label 7950 4700 0    50   ~ 0
MD16
Text Label 7950 4900 0    50   ~ 0
MD18
Text Label 7950 5000 0    50   ~ 0
MD19
Text Label 7950 5200 0    50   ~ 0
MD21
Text Label 7950 5400 0    50   ~ 0
MD23
Text Label 7950 5600 0    50   ~ 0
MD25
Text Label 7950 5100 0    50   ~ 0
MD20
Text Label 7950 5300 0    50   ~ 0
MD22
Text Label 7950 5500 0    50   ~ 0
MD24
Text Label 7950 5700 0    50   ~ 0
MD26
Text Label 6800 5700 0    50   ~ 0
MD27
Text Label 6800 5500 0    50   ~ 0
MD29
Text Label 6800 5300 0    50   ~ 0
MD31
Text Label 5500 5600 0    50   ~ 0
MD33
Text Label 5500 5400 0    50   ~ 0
MD35
Text Label 6800 3300 0    50   ~ 0
MA33
Text Label 6800 3700 0    50   ~ 0
MA29
Wire Wire Line
	6500 4200 7050 4200
Wire Wire Line
	6500 4300 7050 4300
Wire Wire Line
	6500 4400 7050 4400
Wire Wire Line
	6500 4500 7050 4500
Wire Wire Line
	6500 4800 7050 4800
Wire Wire Line
	6500 4900 7050 4900
Text Label 6800 5600 0    50   ~ 0
MD28
Text Label 6800 5400 0    50   ~ 0
MD30
Text Label 5500 5700 0    50   ~ 0
MD32
Text Label 5500 5500 0    50   ~ 0
MD34
Text Label 9200 5600 0    50   ~ 0
SPIMOSI
Text Label 6800 3500 0    50   ~ 0
MA31
Text Label 6800 4300 0    50   ~ 0
MA23
Text Label 6800 3600 0    50   ~ 0
MA30
Text Label 6800 4500 0    50   ~ 0
MA21
Text Label 6800 3400 0    50   ~ 0
MA32
Text Label 6800 3800 0    50   ~ 0
MA28
Text Label 6800 4900 0    50   ~ 0
MA17
Text Label 6800 4700 0    50   ~ 0
MA19
$Comp
L power:GND #PWR0122
U 1 1 5D523618
P 6950 2800
F 0 "#PWR0122" H 6950 2550 50  0001 C CNN
F 1 "GND" H 6950 2850 50  0000 C CNN
F 2 "" H 6950 2800 50  0001 C CNN
F 3 "" H 6950 2800 50  0001 C CNN
	1    6950 2800
	1    0    0    -1  
$EndComp
Text Label 5450 3700 0    50   ~ 0
~EIRQ
Text Label 9200 4700 0    50   ~ 0
DD8
Text Label 9200 4500 0    50   ~ 0
DD6
Text Label 9200 4400 0    50   ~ 0
DD5
Text Label 9200 4600 0    50   ~ 0
DD7
Text Label 9200 4300 0    50   ~ 0
DD4
Text Label 9200 4200 0    50   ~ 0
DD3
Text Label 9200 4100 0    50   ~ 0
DD2
Text Label 9200 4000 0    50   ~ 0
DD1
Text Label 9200 4900 0    50   ~ 0
DD10
Text Label 9200 5000 0    50   ~ 0
DD11
Text Label 9200 5100 0    50   ~ 0
DD12
Text Label 9200 5200 0    50   ~ 0
DD13
Text Label 9200 3900 0    50   ~ 0
DD0
Text Label 9200 5700 0    50   ~ 0
SPIMISO
Text Label 9200 3600 0    50   ~ 0
~DWR
Text Label 9200 3800 0    50   ~ 0
DIRQ
Text Label 9200 3400 0    50   ~ 0
DA2
Text Label 5450 5100 0    50   ~ 0
~MCS1
Text Label 5450 5000 0    50   ~ 0
~MOE1
Text Label 9200 5300 0    50   ~ 0
DD14
Text Label 9200 5400 0    50   ~ 0
DD15
Text Label 9200 3500 0    50   ~ 0
~DRD
Text Label 9200 3700 0    50   ~ 0
~DDAK
Text Label 9200 3300 0    50   ~ 0
DA1
Text Label 9200 3200 0    50   ~ 0
DA0
Text Label 1300 5900 0    50   ~ 0
~DCS0
Text Label 5450 5300 0    50   ~ 0
KCLK
Text Label 5450 3100 0    50   ~ 0
TTXD
Text Label 5450 3300 0    50   ~ 0
TXRD
Text Label 1300 5800 0    50   ~ 0
~DCS1
Text Label 5450 3500 0    50   ~ 0
RSDA
Text Label 5450 3600 0    50   ~ 0
RSCL
Text Label 6800 4600 0    50   ~ 0
MA20
Text Label 6800 3900 0    50   ~ 0
MA27
Text Label 6800 4100 0    50   ~ 0
MA25
Text Label 6800 4800 0    50   ~ 0
MA18
Text Label 6800 4000 0    50   ~ 0
MA26
Text Label 6800 5200 0    50   ~ 0
~MCS0
Text Label 7950 3200 0    50   ~ 0
MD1
Text Label 6800 5000 0    50   ~ 0
~MWE0
Text Label 6800 4200 0    50   ~ 0
MA24
Text Label 6800 3100 0    50   ~ 0
MA35
Text Label 6800 4400 0    50   ~ 0
MA22
Text Label 6800 5100 0    50   ~ 0
~MOE0
Text Label 7950 3100 0    50   ~ 0
MD0
Text Label 7950 3300 0    50   ~ 0
MD2
Text Label 7950 3400 0    50   ~ 0
MD3
Text Label 7950 3600 0    50   ~ 0
MD5
Text Label 7950 3500 0    50   ~ 0
MD4
Text Label 7950 3700 0    50   ~ 0
MD6
Entry Wire Line
	7050 4500 7150 4400
Entry Wire Line
	7050 4400 7150 4300
Entry Wire Line
	7050 4300 7150 4200
Entry Wire Line
	7050 4000 7150 3900
Entry Wire Line
	7050 3900 7150 3800
Entry Wire Line
	7050 3800 7150 3700
Entry Wire Line
	7050 3700 7150 3600
Entry Wire Line
	7050 3600 7150 3500
Entry Wire Line
	7050 3500 7150 3400
Entry Wire Line
	7050 3400 7150 3300
Entry Wire Line
	7050 3300 7150 3200
Entry Wire Line
	7050 3200 7150 3100
Entry Wire Line
	7050 4200 7150 4100
Entry Wire Line
	7050 3100 7150 3000
Text HLabel 5250 5350 0    50   BiDi ~ 0
MD[0..35]
Entry Wire Line
	7700 3900 7800 3800
Entry Wire Line
	7700 4000 7800 3900
Entry Wire Line
	7700 4100 7800 4000
Entry Wire Line
	7700 4200 7800 4100
Entry Wire Line
	7700 4300 7800 4200
Text HLabel 7300 5200 2    50   Output ~ 0
~MCS0
Text HLabel 7300 5100 2    50   Output ~ 0
~MOE0
Text HLabel 7300 5000 2    50   Output ~ 0
~MWE0
Text HLabel 7550 2850 2    50   Output ~ 0
MA[17..35]
Text HLabel 4550 3700 0    50   Input ~ 0
~EIRQ
Text HLabel 9850 3500 2    50   Output ~ 0
~DRD
Text HLabel 9850 3700 2    50   Output ~ 0
~DDAK
Text HLabel 1200 5900 0    50   Output ~ 0
~DCS0
Entry Wire Line
	9400 5200 9500 5100
Entry Wire Line
	9400 5300 9500 5200
Text HLabel 9850 3200 2    50   Output ~ 0
DA[0..2]
Wire Wire Line
	8850 3500 9400 3500
Entry Wire Line
	9400 5000 9500 4900
Entry Wire Line
	9400 5100 9500 5000
Entry Wire Line
	9400 5400 9500 5300
Text HLabel 9900 5500 2    50   Output ~ 0
SPICLK
Text HLabel 1200 5800 0    50   Output ~ 0
~DCS1
Text HLabel 9850 3800 2    50   Input ~ 0
DIRQ
Text HLabel 9850 3600 2    50   Output ~ 0
~DWR
Text HLabel 9900 5700 2    50   Input ~ 0
SPIMISO
Text HLabel 9900 5600 2    50   Output ~ 0
SPIMOSI
Text HLabel 9850 3100 2    50   Output ~ 0
~DRES
Text HLabel 1200 5700 0    50   Output ~ 0
~SDCS
$Comp
L Connector:Barrel_Jack J1
U 1 1 5D5862F5
P 5200 1250
F 0 "J1" H 5255 1575 50  0000 C CNN
F 1 "Barrel_Jack" H 5255 1484 50  0000 C CNN
F 2 "Connector_BarrelJack:BarrelJack_Horizontal" H 5250 1210 50  0001 C CNN
F 3 "~" H 5250 1210 50  0001 C CNN
	1    5200 1250
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:LT1086-3.3 U?
U 1 1 5D5A03EE
P 6800 1150
AR Path="/5D5A03EE" Ref="U?"  Part="1" 
AR Path="/5D514CEA/5D5A03EE" Ref="U1"  Part="1" 
F 0 "U1" H 6800 1392 50  0000 C CNN
F 1 "LT1086-3.3" H 6800 1301 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Horizontal_TabDown" H 6800 1400 50  0001 C CIN
F 3 "http://cds.linear.com/docs/en/datasheet/1086ffs.pdf" H 6800 1150 50  0001 C CNN
	1    6800 1150
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5D5A03F5
P 5650 950
AR Path="/5D5A03F5" Ref="#PWR?"  Part="1" 
AR Path="/5D514CEA/5D5A03F5" Ref="#PWR0130"  Part="1" 
F 0 "#PWR0130" H 5650 800 50  0001 C CNN
F 1 "+5V" H 5665 1123 50  0000 C CNN
F 2 "" H 5650 950 50  0001 C CNN
F 3 "" H 5650 950 50  0001 C CNN
	1    5650 950 
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5D5A03FB
P 5900 1100
AR Path="/5D5A03FB" Ref="#FLG?"  Part="1" 
AR Path="/5D514CEA/5D5A03FB" Ref="#FLG0101"  Part="1" 
F 0 "#FLG0101" H 5900 1175 50  0001 C CNN
F 1 "PWR_FLAG" H 5900 1274 50  0000 C CNN
F 2 "" H 5900 1100 50  0001 C CNN
F 3 "~" H 5900 1100 50  0001 C CNN
	1    5900 1100
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 5D5A0401
P 6150 950
AR Path="/5D5A0401" Ref="#PWR?"  Part="1" 
AR Path="/5D514CEA/5D5A0401" Ref="#PWR0149"  Part="1" 
F 0 "#PWR0149" H 6150 800 50  0001 C CNN
F 1 "VCC" H 6167 1123 50  0000 C CNN
F 2 "" H 6150 950 50  0001 C CNN
F 3 "" H 6150 950 50  0001 C CNN
	1    6150 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 1150 5650 1150
Wire Wire Line
	5650 950  5650 1150
Connection ~ 5650 1150
Wire Wire Line
	6150 950  6150 1150
Connection ~ 6150 1150
$Comp
L power:+3.3V #PWR?
U 1 1 5D5A0411
P 7450 950
AR Path="/5D5A0411" Ref="#PWR?"  Part="1" 
AR Path="/5D514CEA/5D5A0411" Ref="#PWR0150"  Part="1" 
F 0 "#PWR0150" H 7450 800 50  0001 C CNN
F 1 "+3.3V" H 7465 1123 50  0000 C CNN
F 2 "" H 7450 950 50  0001 C CNN
F 3 "" H 7450 950 50  0001 C CNN
	1    7450 950 
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR?
U 1 1 5D5A041D
P 8950 1150
AR Path="/5D5A041D" Ref="#PWR?"  Part="1" 
AR Path="/5D514CEA/5D5A041D" Ref="#PWR0151"  Part="1" 
F 0 "#PWR0151" H 8950 1000 50  0001 C CNN
F 1 "VDD" H 8967 1323 50  0000 C CNN
F 2 "" H 8950 1150 50  0001 C CNN
F 3 "" H 8950 1150 50  0001 C CNN
	1    8950 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 950  7450 1150
Connection ~ 7450 1150
$Comp
L power:GND #PWR?
U 1 1 5D5A042B
P 6600 1650
AR Path="/5D5A042B" Ref="#PWR?"  Part="1" 
AR Path="/5D514CEA/5D5A042B" Ref="#PWR0152"  Part="1" 
F 0 "#PWR0152" H 6600 1400 50  0001 C CNN
F 1 "GND" H 6605 1477 50  0000 C CNN
F 2 "" H 6600 1650 50  0001 C CNN
F 3 "" H 6600 1650 50  0001 C CNN
	1    6600 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 1350 5500 1550
Connection ~ 6600 1550
Wire Wire Line
	6600 1550 6600 1650
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5D5A0437
P 6000 1550
AR Path="/5D5A0437" Ref="#FLG?"  Part="1" 
AR Path="/5D514CEA/5D5A0437" Ref="#FLG0103"  Part="1" 
F 0 "#FLG0103" H 6000 1625 50  0001 C CNN
F 1 "PWR_FLAG" H 5750 1600 50  0000 C CNN
F 2 "" H 6000 1550 50  0001 C CNN
F 3 "~" H 6000 1550 50  0001 C CNN
	1    6000 1550
	1    0    0    -1  
$EndComp
Connection ~ 6000 1550
Wire Wire Line
	6000 1550 6350 1550
Wire Wire Line
	5650 1150 5900 1150
Wire Wire Line
	5900 1100 5900 1150
Connection ~ 5900 1150
Wire Wire Line
	5900 1150 6150 1150
Wire Wire Line
	7450 1150 8100 1150
$Comp
L Device:C C101
U 1 1 5D87F61A
P 7350 1350
F 0 "C101" H 7465 1396 50  0000 L CNN
F 1 "10uF (TANT)" H 7465 1305 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_Tantal_D4.5mm_P2.50mm" H 7388 1200 50  0001 C CNN
F 3 "~" H 7350 1350 50  0001 C CNN
	1    7350 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C102
U 1 1 5D8A5CA0
P 6350 1300
F 0 "C102" H 6050 1350 50  0000 L CNN
F 1 "10uF (TANT)" H 5800 1250 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_Tantal_D4.5mm_P2.50mm" H 6388 1150 50  0001 C CNN
F 3 "~" H 6350 1300 50  0001 C CNN
	1    6350 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 1150 6350 1150
Connection ~ 6350 1150
Wire Wire Line
	6350 1150 6500 1150
Wire Wire Line
	6350 1450 6350 1550
Connection ~ 6350 1550
Wire Wire Line
	6350 1550 6600 1550
Wire Wire Line
	7350 1550 6800 1550
Connection ~ 6800 1550
Wire Wire Line
	6800 1550 6600 1550
Wire Wire Line
	6800 1450 6800 1550
$Comp
L power:VCC #PWR0124
U 1 1 5DA35AC6
P 6950 6050
F 0 "#PWR0124" H 6950 5900 50  0001 C CNN
F 1 "VCC" H 6950 6000 50  0000 C CNN
F 2 "" H 6950 6050 50  0001 C CNN
F 3 "" H 6950 6050 50  0001 C CNN
	1    6950 6050
	1    0    0    -1  
$EndComp
Text Label 1300 5700 0    50   ~ 0
~SDCS
Text Label 5450 3200 0    50   ~ 0
~TRTS
Text Notes 8050 2900 0    50   ~ 0
(3.3V)
Text Label 5450 5200 0    50   ~ 0
KDAT
Text HLabel 4550 4900 0    50   Output ~ 0
~MWE1
Text HLabel 4550 5000 0    50   Output ~ 0
~MOE1
Text HLabel 4550 5100 0    50   Output ~ 0
~MCS1
Text Label 5450 4900 0    50   ~ 0
~MWE1
Text HLabel 4550 5300 0    50   BiDi ~ 0
KCLK
Text HLabel 4550 5200 0    50   BiDi ~ 0
KDAT
Wire Wire Line
	8850 5100 9400 5100
Wire Wire Line
	6500 5300 7050 5300
Wire Wire Line
	4550 5000 6000 5000
Wire Wire Line
	4550 4900 6000 4900
Wire Wire Line
	4550 4800 6000 4800
Wire Wire Line
	4550 4700 6000 4700
Wire Wire Line
	4550 4600 6000 4600
Wire Wire Line
	4550 4500 6000 4500
Wire Wire Line
	4550 4400 6000 4400
Wire Wire Line
	4550 4300 6000 4300
Wire Wire Line
	4550 4200 6000 4200
$Comp
L Connector_Generic:Conn_02x32_Odd_Even J8
U 1 1 5E543D84
P 6300 4300
F 0 "J8" H 6350 6017 50  0000 C CNN
F 1 "Conn_02x32_Odd_Even" H 6350 5926 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x32_P2.54mm_Vertical" H 6300 4300 50  0001 C CNN
F 3 "~" H 6300 4300 50  0001 C CNN
	1    6300 4300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5450 5400 6000 5400
Wire Wire Line
	5450 5500 6000 5500
Entry Wire Line
	7700 4400 7800 4300
Entry Wire Line
	7700 4500 7800 4400
Entry Wire Line
	5350 5600 5450 5500
Entry Wire Line
	5350 5700 5450 5600
Wire Wire Line
	5450 5600 6000 5600
Wire Wire Line
	6500 5500 7050 5500
Wire Wire Line
	6500 5600 7050 5600
Wire Wire Line
	6500 5400 7050 5400
Wire Wire Line
	6500 5900 6600 5900
Wire Wire Line
	6000 5900 5900 5900
Wire Wire Line
	5900 5900 5900 6050
Wire Wire Line
	5900 6050 6600 6050
Wire Wire Line
	6600 6050 6600 5900
$Comp
L power:GND #PWR0117
U 1 1 5E76D726
P 6700 6150
F 0 "#PWR0117" H 6700 5900 50  0001 C CNN
F 1 "GND" H 6700 6200 50  0000 C CNN
F 2 "" H 6700 6150 50  0001 C CNN
F 3 "" H 6700 6150 50  0001 C CNN
	1    6700 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 5800 5800 5800
Wire Wire Line
	5800 5800 5800 6150
Wire Wire Line
	6500 5800 6700 5800
Wire Wire Line
	5800 6150 6700 6150
Wire Wire Line
	6700 5800 6700 6150
Wire Wire Line
	6000 5700 5450 5700
Wire Wire Line
	6500 5700 7050 5700
Text Label 6800 3200 0    50   ~ 0
MA34
Wire Wire Line
	5800 2500 6700 2500
Wire Wire Line
	6700 3000 6500 3000
Wire Wire Line
	5800 3000 6000 3000
Wire Wire Line
	6500 3400 7050 3400
Wire Wire Line
	4550 4100 6000 4100
Wire Wire Line
	6500 4100 7050 4100
Wire Wire Line
	6500 4600 7050 4600
Wire Wire Line
	6500 4700 7050 4700
Entry Wire Line
	7050 4600 7150 4500
Entry Wire Line
	7050 4700 7150 4600
Entry Wire Line
	7050 4800 7150 4700
Entry Wire Line
	7050 4100 7150 4000
Entry Wire Line
	7050 5700 7150 5800
Entry Wire Line
	7050 5600 7150 5700
Entry Wire Line
	7050 5500 7150 5600
Entry Wire Line
	7050 5400 7150 5500
Entry Wire Line
	7050 5300 7150 5400
Entry Wire Line
	5350 5800 5450 5700
Wire Wire Line
	6600 6050 6950 6050
Connection ~ 6600 6050
Wire Wire Line
	5800 2500 5800 2800
Wire Wire Line
	6700 2500 6700 2800
Wire Wire Line
	5800 2800 6000 2800
Connection ~ 5800 2800
Wire Wire Line
	5800 2800 5800 3000
Wire Wire Line
	6500 2800 6700 2800
Connection ~ 6700 2800
Wire Wire Line
	6700 2800 6700 3000
Wire Wire Line
	6700 2800 6950 2800
$Comp
L Connector_Generic:Conn_02x32_Odd_Even J7
U 1 1 5ED8AA15
P 8650 4300
F 0 "J7" H 8700 6017 50  0000 C CNN
F 1 "Conn_02x32_Odd_Even" H 8700 5926 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x32_P2.54mm_Vertical" H 8650 4300 50  0001 C CNN
F 3 "~" H 8650 4300 50  0001 C CNN
	1    8650 4300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8350 5900 8250 5900
Wire Wire Line
	8250 5900 8250 6050
Wire Wire Line
	8350 5800 8150 5800
Wire Wire Line
	8150 5800 8150 6150
Wire Wire Line
	8150 6150 9050 6150
$Comp
L power:VCC #PWR0120
U 1 1 5EE06D95
P 7950 6000
F 0 "#PWR0120" H 7950 5850 50  0001 C CNN
F 1 "VCC" H 7850 6100 50  0000 C CNN
F 2 "" H 7950 6000 50  0001 C CNN
F 3 "" H 7950 6000 50  0001 C CNN
	1    7950 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 6050 8950 6050
Wire Wire Line
	8850 5900 8950 5900
Wire Wire Line
	8950 5900 8950 6050
Wire Wire Line
	8850 5800 9050 5800
Wire Wire Line
	9050 5800 9050 6150
Text Label 9200 4800 0    50   ~ 0
DD9
Wire Bus Line
	9850 3200 9500 3200
Entry Wire Line
	9400 3200 9500 3300
Entry Wire Line
	9400 3300 9500 3400
Entry Wire Line
	9400 3400 9500 3500
Wire Wire Line
	8850 4600 9400 4600
Wire Wire Line
	8850 4700 9400 4700
Wire Wire Line
	8850 4800 9400 4800
Wire Wire Line
	8850 4900 9400 4900
Text HLabel 4550 3500 0    50   BiDi ~ 0
RSDA
Text HLabel 4550 3600 0    50   Output ~ 0
RSCL
Wire Wire Line
	8850 5000 9400 5000
Wire Wire Line
	8850 5300 9400 5300
Wire Wire Line
	8850 5200 9400 5200
Wire Wire Line
	8850 5400 9400 5400
Text Notes 8900 2900 0    50   ~ 0
(3.3V)
Text Label 5450 4100 0    50   ~ 0
VIDEOB0
Text Label 5450 4200 0    50   ~ 0
VIDEOB1
Text Label 5450 4300 0    50   ~ 0
VIDEOG0
Text Label 5450 4400 0    50   ~ 0
VIDEOG1
Text Label 5450 4500 0    50   ~ 0
VIDEOR0
Text Label 5450 4600 0    50   ~ 0
VIDEOR1
Text Label 5450 4700 0    50   ~ 0
VSYNC
Text Label 5450 4800 0    50   ~ 0
HSYNC
$Comp
L 74xx:74LS138 U2
U 1 1 5F47CED1
P 2100 5600
F 0 "U2" H 2100 6381 50  0000 C CNN
F 1 "74HC138" H 2100 6290 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket_LongPads" H 2100 5600 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS138" H 2100 5600 50  0001 C CNN
	1    2100 5600
	-1   0    0    -1  
$EndComp
Text Label 1300 5500 0    50   ~ 0
~ECS
Text HLabel 1200 5500 0    50   Output ~ 0
~ECS
Wire Wire Line
	1200 5500 1600 5500
Wire Wire Line
	1200 5700 1600 5700
Wire Wire Line
	1200 5800 1600 5800
Wire Wire Line
	1200 5900 1600 5900
NoConn ~ 1600 6000
Text Label 5450 3800 0    50   ~ 0
CS0
Text Label 5450 3900 0    50   ~ 0
CS1
NoConn ~ 1600 5300
NoConn ~ 1600 5400
Wire Wire Line
	8150 2500 9050 2500
Wire Wire Line
	8150 2500 8150 2800
Wire Wire Line
	9050 2500 9050 2800
Wire Wire Line
	8150 2800 8350 2800
Wire Wire Line
	8150 2800 8150 3000
Wire Wire Line
	8150 3000 8350 3000
Connection ~ 8150 2800
Wire Wire Line
	8850 2800 9050 2800
Wire Wire Line
	8850 3000 9050 3000
Wire Wire Line
	9050 3000 9050 2800
Connection ~ 9050 2800
$Comp
L power:GND #PWR0121
U 1 1 5F7A0A01
P 9300 2800
F 0 "#PWR0121" H 9300 2550 50  0001 C CNN
F 1 "GND" H 9300 2850 50  0000 C CNN
F 2 "" H 9300 2800 50  0001 C CNN
F 3 "" H 9300 2800 50  0001 C CNN
	1    9300 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 2800 9300 2800
NoConn ~ 6500 2900
NoConn ~ 6000 2900
NoConn ~ 8350 2900
NoConn ~ 8850 2900
Text HLabel 4550 4100 0    50   Input ~ 0
VIDEOB0
Text HLabel 4550 4200 0    50   Input ~ 0
VIDEOB1
Text HLabel 4550 4300 0    50   Input ~ 0
VIDEOG0
Text HLabel 4550 4400 0    50   Input ~ 0
VIDEOG1
Text HLabel 4550 4500 0    50   Input ~ 0
VIDEOR0
Text HLabel 4550 4600 0    50   Input ~ 0
VIDEOR1
Text HLabel 4550 4700 0    50   Input ~ 0
VSYNC
Text HLabel 4550 4800 0    50   Input ~ 0
HSYNC
Wire Wire Line
	7950 6000 7950 6050
Wire Wire Line
	7950 6050 8250 6050
Connection ~ 8250 6050
$Comp
L power:VDD #PWR0126
U 1 1 5F853BCD
P 2700 5000
F 0 "#PWR0126" H 2700 4850 50  0001 C CNN
F 1 "VDD" H 2717 5173 50  0000 C CNN
F 2 "" H 2700 5000 50  0001 C CNN
F 3 "" H 2700 5000 50  0001 C CNN
	1    2700 5000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0129
U 1 1 5F94CA51
P 9050 6150
F 0 "#PWR0129" H 9050 5900 50  0001 C CNN
F 1 "GND" H 9055 5977 50  0000 C CNN
F 2 "" H 9050 6150 50  0001 C CNN
F 3 "" H 9050 6150 50  0001 C CNN
	1    9050 6150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C103
U 1 1 5E9E10D0
P 8100 1350
F 0 "C103" H 8215 1396 50  0000 L CNN
F 1 "0.1uF" H 8215 1305 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 8138 1200 50  0001 C CNN
F 3 "~" H 8100 1350 50  0001 C CNN
	1    8100 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 1500 7350 1550
Wire Wire Line
	7100 1150 7350 1150
Wire Wire Line
	7350 1200 7350 1150
Connection ~ 7350 1150
Wire Wire Line
	7350 1150 7450 1150
Wire Wire Line
	8100 1200 8100 1150
Connection ~ 8100 1150
Wire Wire Line
	7350 1550 8100 1550
Wire Wire Line
	8100 1550 8100 1500
Connection ~ 7350 1550
Text Label 5450 3400 0    50   ~ 0
~TCTS
Text Label 9200 3100 0    50   ~ 0
~DRES
Wire Wire Line
	8850 4500 9400 4500
Wire Wire Line
	8850 4400 9400 4400
Wire Wire Line
	8850 4300 9400 4300
Wire Wire Line
	8850 4200 9400 4200
Entry Wire Line
	9400 4400 9500 4300
Entry Wire Line
	9400 4500 9500 4400
Entry Wire Line
	9400 4200 9500 4100
Entry Wire Line
	9400 4300 9500 4200
Entry Wire Line
	9400 4900 9500 4800
Entry Wire Line
	9400 4600 9500 4500
Entry Wire Line
	9400 4800 9500 4700
Entry Wire Line
	9400 4700 9500 4600
Text Label 9200 5500 0    50   ~ 0
SPICLK
Wire Bus Line
	9500 5400 9900 5400
Wire Wire Line
	9400 3800 9400 3750
Wire Wire Line
	9400 3750 9600 3750
Wire Wire Line
	9600 3750 9600 3800
Wire Wire Line
	9600 3800 9850 3800
Wire Wire Line
	8850 3700 9850 3700
Wire Wire Line
	9400 3500 9400 3550
Wire Wire Line
	9400 3550 9600 3550
Wire Wire Line
	9600 3550 9600 3500
Wire Wire Line
	9600 3500 9850 3500
Wire Wire Line
	8850 3600 9850 3600
Text HLabel 9900 5400 2    50   Output ~ 0
DD[0..15]
Wire Wire Line
	2100 6300 2700 6300
Wire Wire Line
	2100 5000 2700 5000
Wire Wire Line
	2700 5000 2700 5800
Wire Wire Line
	2700 5800 2600 5800
Connection ~ 2700 5000
$Comp
L power:GND #PWR0125
U 1 1 5EE7959E
P 2700 6300
F 0 "#PWR0125" H 2700 6050 50  0001 C CNN
F 1 "GND" H 2705 6127 50  0000 C CNN
F 2 "" H 2700 6300 50  0001 C CNN
F 3 "" H 2700 6300 50  0001 C CNN
	1    2700 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 5900 2700 5900
Wire Wire Line
	2700 5900 2700 6000
Connection ~ 2700 6300
Wire Wire Line
	2600 6000 2700 6000
Connection ~ 2700 6000
Wire Wire Line
	2700 6000 2700 6300
Entry Wire Line
	7700 4600 7800 4500
Entry Wire Line
	7700 4700 7800 4600
Entry Wire Line
	7700 4800 7800 4700
Entry Wire Line
	7700 4900 7800 4800
Entry Wire Line
	7700 5000 7800 4900
Entry Wire Line
	7700 5100 7800 5000
Entry Wire Line
	7700 5200 7800 5100
Entry Wire Line
	7700 5300 7800 5200
Entry Wire Line
	7700 5400 7800 5300
Entry Wire Line
	7700 5500 7800 5400
Entry Wire Line
	7700 3200 7800 3100
Entry Wire Line
	7700 3300 7800 3200
Entry Wire Line
	7700 3400 7800 3300
Entry Wire Line
	7700 3500 7800 3400
Entry Wire Line
	7700 3600 7800 3500
Entry Wire Line
	7700 3700 7800 3600
Entry Wire Line
	7700 3800 7800 3700
Wire Bus Line
	5250 5350 5350 5350
Entry Wire Line
	7050 4900 7150 4800
Wire Bus Line
	7150 2850 7550 2850
$Comp
L Interface_UART:MAX3232 U3
U 1 1 5F07FDF8
P 3050 2700
F 0 "U3" H 3050 4081 50  0000 C CNN
F 1 "MAX3232" H 3050 3990 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket_LongPads" H 3100 1650 50  0001 L CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX3222-MAX3241.pdf" H 3050 2800 50  0001 C CNN
	1    3050 2700
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C105
U 1 1 5F0C538F
P 2150 1950
F 0 "C105" H 1850 2000 50  0000 L CNN
F 1 "0.1uF" H 1850 1900 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 2188 1800 50  0001 C CNN
F 3 "~" H 2150 1950 50  0001 C CNN
	1    2150 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C108
U 1 1 5F0C6595
P 3950 1950
F 0 "C108" H 3650 2000 50  0000 L CNN
F 1 "0.1uF" H 3650 1900 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 3988 1800 50  0001 C CNN
F 3 "~" H 3950 1950 50  0001 C CNN
	1    3950 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C106
U 1 1 5F0C6E6C
P 1850 2250
F 0 "C106" H 1550 2350 50  0000 L CNN
F 1 "0.1uF" H 1550 2250 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 1888 2100 50  0001 C CNN
F 3 "~" H 1850 2250 50  0001 C CNN
	1    1850 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C107
U 1 1 5F0C781B
P 1850 2650
F 0 "C107" H 1550 2700 50  0000 L CNN
F 1 "0.1uF" H 1550 2600 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 1888 2500 50  0001 C CNN
F 3 "~" H 1850 2650 50  0001 C CNN
	1    1850 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 2100 2250 2100
Wire Wire Line
	2150 1800 2250 1800
Wire Wire Line
	1850 2100 2050 2100
Wire Wire Line
	2050 2100 2050 2300
Wire Wire Line
	2050 2300 2250 2300
Wire Wire Line
	1850 2800 2050 2800
Wire Wire Line
	2050 2800 2050 2600
Wire Wire Line
	2050 2600 2250 2600
Wire Wire Line
	1850 2400 1850 2450
Wire Wire Line
	1850 2450 1450 2450
Connection ~ 1850 2450
Wire Wire Line
	1850 2450 1850 2500
$Comp
L power:GND #PWR0144
U 1 1 5F14C6BC
P 1450 2450
F 0 "#PWR0144" H 1450 2200 50  0001 C CNN
F 1 "GND" H 1450 2500 50  0000 C CNN
F 2 "" H 1450 2450 50  0001 C CNN
F 3 "" H 1450 2450 50  0001 C CNN
	1    1450 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 1800 3950 1800
Wire Wire Line
	3850 2100 3950 2100
Connection ~ 9050 6150
Wire Wire Line
	3850 3200 4150 3200
Text Label 3900 2800 0    50   ~ 0
TTXD
Text Label 3900 3000 0    50   ~ 0
~TRTS
Text Label 3900 3200 0    50   ~ 0
TXRD
Text Label 3900 3400 0    50   ~ 0
~TCTS
Wire Wire Line
	4250 3000 3850 3000
Wire Wire Line
	2250 3000 2150 3000
Wire Wire Line
	2150 3000 2150 3100
Wire Wire Line
	2250 2800 2150 2800
Wire Wire Line
	2150 2800 2150 2900
$Comp
L power:GND #PWR0145
U 1 1 5F267C35
P 1800 3900
F 0 "#PWR0145" H 1800 3650 50  0001 C CNN
F 1 "GND" H 1800 3950 50  0000 C CNN
F 2 "" H 1800 3900 50  0001 C CNN
F 3 "" H 1800 3900 50  0001 C CNN
	1    1800 3900
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR0146
U 1 1 5F337183
P 3350 1500
F 0 "#PWR0146" H 3350 1350 50  0001 C CNN
F 1 "VDD" H 3367 1673 50  0000 C CNN
F 2 "" H 3350 1500 50  0001 C CNN
F 3 "" H 3350 1500 50  0001 C CNN
	1    3350 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 1500 3350 1500
$Comp
L Device:C C104
U 1 1 5F366E22
P 8950 1350
F 0 "C104" H 9065 1396 50  0000 L CNN
F 1 "0.1uF" H 9065 1305 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 8988 1200 50  0001 C CNN
F 3 "~" H 8950 1350 50  0001 C CNN
	1    8950 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 1150 8950 1200
Wire Wire Line
	8100 1150 8950 1150
Wire Wire Line
	8100 1550 8950 1550
Wire Wire Line
	8950 1550 8950 1500
Connection ~ 8100 1550
Connection ~ 8950 1150
Text Notes 8200 5500 0    50   ~ 0
R14
Text Notes 8200 5400 0    50   ~ 0
R15
Text Notes 8200 5700 0    50   ~ 0
T12
Text Notes 8200 5600 0    50   ~ 0
T13
Text Notes 8200 3100 0    50   ~ 0
E12
Text Notes 8200 3200 0    50   ~ 0
B15
Text Notes 8200 3300 0    50   ~ 0
C15
Text Notes 8200 3400 0    50   ~ 0
D14
Text Notes 8200 3500 0    50   ~ 0
E15
Text Notes 8200 3600 0    50   ~ 0
F15
Text Notes 8200 3700 0    50   ~ 0
G11
Text Notes 8200 3800 0    50   ~ 0
F14
Text Notes 8200 3900 0    50   ~ 0
G16
Text Notes 8200 4000 0    50   ~ 0
H15
Text Notes 8200 4100 0    50   ~ 0
G12
Text Notes 8200 4200 0    50   ~ 0
H13
Text Notes 8200 4300 0    50   ~ 0
J14
Text Notes 8200 4400 0    50   ~ 0
J11
Text Notes 8200 4500 0    50   ~ 0
K14
Text Notes 8200 4600 0    50   ~ 0
K15
Text Notes 8200 4700 0    50   ~ 0
L16
Text Notes 8200 4800 0    50   ~ 0
K11
Text Notes 8200 4900 0    50   ~ 0
M15
Text Notes 8200 5000 0    50   ~ 0
N14
Text Notes 8200 5100 0    50   ~ 0
M13
Text Notes 8200 5200 0    50   ~ 0
L12
Text Notes 8200 5300 0    50   ~ 0
P15
Text Notes 8900 3100 0    50   ~ 0
E13
Text Notes 8900 3200 0    50   ~ 0
B16
Text Notes 8900 3300 0    50   ~ 0
C16
Text Notes 8900 3400 0    50   ~ 0
D16
Text Notes 8900 3500 0    50   ~ 0
E16
Text Notes 8900 3600 0    50   ~ 0
F16
Text Notes 8900 3700 0    50   ~ 0
F12
Text Notes 8900 3800 0    50   ~ 0
F13
Text Notes 8900 3900 0    50   ~ 0
G14
Text Notes 8900 4000 0    50   ~ 0
H16
Text Notes 8900 4100 0    50   ~ 0
H11
Text Notes 8900 4200 0    50   ~ 0
H14
Text Notes 8900 4300 0    50   ~ 0
J16
Text Notes 8900 4400 0    50   ~ 0
J12
Text Notes 8900 4500 0    50   ~ 0
J13
Text Notes 8900 4600 0    50   ~ 0
K16
Text Notes 8900 4700 0    50   ~ 0
L14
Text Notes 8900 4800 0    50   ~ 0
K12
Text Notes 8900 4900 0    50   ~ 0
M16
Text Notes 8900 5000 0    50   ~ 0
N16
Text Notes 8900 5100 0    50   ~ 0
M14
Text Notes 8900 5200 0    50   ~ 0
L13
Text Notes 8900 5300 0    50   ~ 0
P16
Text Notes 8900 5400 0    50   ~ 0
R16
Text Notes 8900 5500 0    50   ~ 0
T15
Text Notes 8900 5600 0    50   ~ 0
T14
Text Notes 8900 5700 0    50   ~ 0
R12
Text Notes 5800 3100 0    50   ~ 0
A14
Text Notes 5800 3200 0    50   ~ 0
C13
Text Notes 5800 3300 0    50   ~ 0
B12
Text Notes 5800 3400 0    50   ~ 0
C11
Text Notes 5800 3500 0    50   ~ 0
B10
Text Notes 5800 3600 0    50   ~ 0
C9
Text Notes 5800 3700 0    50   ~ 0
B8
Text Notes 5800 3800 0    50   ~ 0
C7
Text Notes 5800 3900 0    50   ~ 0
B6
Text Notes 5800 4000 0    50   ~ 0
B5
Text Notes 5800 4100 0    50   ~ 0
E10
Text Notes 5800 4200 0    50   ~ 0
E11
Text Notes 5800 4300 0    50   ~ 0
F9
Text Notes 5800 4400 0    50   ~ 0
C8
Text Notes 5800 4500 0    50   ~ 0
E7
Text Notes 5800 4600 0    50   ~ 0
F7
Text Notes 5800 4700 0    50   ~ 0
D6
Text Notes 5800 4800 0    50   ~ 0
P4
Text Notes 5800 4900 0    50   ~ 0
P5
Text Notes 5800 5000 0    50   ~ 0
M7
Text Notes 5800 5100 0    50   ~ 0
N8
Text Notes 5800 5200 0    50   ~ 0
P9
Text Notes 5800 5300 0    50   ~ 0
T5
Text Notes 5800 5400 0    50   ~ 0
T6
Text Notes 5800 5500 0    50   ~ 0
N9
Text Notes 5800 5600 0    50   ~ 0
M10
Text Notes 5800 5700 0    50   ~ 0
P12
Text Notes 6550 3100 0    50   ~ 0
B14
Text Notes 6550 3200 0    50   ~ 0
A13
Text Notes 6550 3300 0    50   ~ 0
A12
Text Notes 6550 3400 0    50   ~ 0
A11
Text Notes 6550 3500 0    50   ~ 0
A9
Text Notes 6550 3600 0    50   ~ 0
A8
Text Notes 6550 3700 0    50   ~ 0
A7
Text Notes 6550 3800 0    50   ~ 0
A6
Text Notes 6550 3900 0    50   ~ 0
A5
Text Notes 6550 4000 0    50   ~ 0
A4
Text Notes 6550 4100 0    50   ~ 0
C10
Text Notes 6550 4200 0    50   ~ 0
F10
Text Notes 6550 4300 0    50   ~ 0
D9
Text Notes 6550 4400 0    50   ~ 0
D8
Text Notes 6550 4500 0    50   ~ 0
E6
Text Notes 6550 4600 0    50   ~ 0
C6
Text Notes 6550 4700 0    50   ~ 0
M6
Text Notes 6550 4800 0    50   ~ 0
N5
Text Notes 6550 4900 0    50   ~ 0
N6
Text Notes 6550 5000 0    50   ~ 0
P6
Text Notes 6550 5100 0    50   ~ 0
L7
Text Notes 6550 5200 0    50   ~ 0
T4
Text Notes 6550 5300 0    50   ~ 0
R5
Text Notes 6550 5400 0    50   ~ 0
T7
Text Notes 6550 5500 0    50   ~ 0
M9
Text Notes 6550 5600 0    50   ~ 0
P11
Text Notes 6550 5700 0    50   ~ 0
M11
Wire Wire Line
	4550 3500 6000 3500
Wire Wire Line
	4550 3600 6000 3600
Wire Wire Line
	4550 3700 6000 3700
Wire Wire Line
	6500 5000 7300 5000
Wire Wire Line
	6500 5100 7300 5100
Wire Wire Line
	6500 5200 7300 5200
Wire Wire Line
	4550 5300 6000 5300
Wire Wire Line
	4550 5200 6000 5200
Wire Wire Line
	4550 5100 6000 5100
Wire Wire Line
	5500 1550 6000 1550
Connection ~ 6700 6150
Wire Wire Line
	7800 3100 8350 3100
Wire Wire Line
	7800 3200 8350 3200
Wire Wire Line
	7800 3300 8350 3300
Wire Wire Line
	7800 3400 8350 3400
Wire Wire Line
	7800 3500 8350 3500
Wire Wire Line
	7800 3600 8350 3600
Wire Wire Line
	7800 3700 8350 3700
Wire Wire Line
	7800 3800 8350 3800
Wire Wire Line
	7800 3900 8350 3900
Wire Wire Line
	7800 4000 8350 4000
Wire Wire Line
	7800 4100 8350 4100
Wire Wire Line
	7800 4200 8350 4200
Wire Wire Line
	7800 4300 8350 4300
Wire Wire Line
	7800 4400 8350 4400
Wire Wire Line
	7800 4500 8350 4500
Wire Wire Line
	7800 4600 8350 4600
Entry Wire Line
	7700 5500 7800 5400
Entry Wire Line
	7700 5600 7800 5500
Entry Wire Line
	7700 5700 7800 5600
Wire Wire Line
	7800 4700 8350 4700
Wire Wire Line
	7800 4800 8350 4800
Wire Wire Line
	7800 4900 8350 4900
Wire Wire Line
	7800 5000 8350 5000
Wire Wire Line
	7800 5100 8350 5100
Wire Wire Line
	7800 5200 8350 5200
Wire Wire Line
	7800 5300 8350 5300
Wire Wire Line
	7800 5400 8350 5400
Wire Wire Line
	7800 5500 8350 5500
Wire Wire Line
	7800 5600 8350 5600
Wire Bus Line
	5350 6350 7150 6350
Connection ~ 7150 6350
Wire Bus Line
	7150 6350 7700 6350
Entry Wire Line
	5350 5500 5450 5400
Entry Wire Line
	7700 5800 7800 5700
Wire Wire Line
	7800 5700 8350 5700
Wire Wire Line
	8850 3100 9850 3100
Wire Wire Line
	8850 3200 9400 3200
Wire Wire Line
	8850 3300 9400 3300
Wire Wire Line
	8850 3400 9400 3400
Wire Wire Line
	8850 3800 9400 3800
Wire Wire Line
	8850 4100 9400 4100
Wire Wire Line
	8850 4000 9400 4000
Wire Wire Line
	8850 3900 9400 3900
Entry Wire Line
	9400 3900 9500 3800
Entry Wire Line
	9400 4000 9500 3900
Entry Wire Line
	9400 4100 9500 4000
Wire Wire Line
	8850 5500 9900 5500
Wire Wire Line
	8850 5600 9900 5600
Wire Wire Line
	8850 5700 9900 5700
Text Label 5450 4000 0    50   ~ 0
CS2
Wire Wire Line
	4100 5500 4100 4000
Wire Wire Line
	4100 4000 6000 4000
Wire Wire Line
	4000 5400 4000 3900
Wire Wire Line
	4000 3900 6000 3900
Wire Wire Line
	3900 5300 3900 3800
Wire Wire Line
	3900 3800 6000 3800
Wire Wire Line
	2600 5500 4100 5500
Wire Wire Line
	2600 5400 4000 5400
Wire Wire Line
	2600 5300 3900 5300
NoConn ~ 1350 3300
NoConn ~ 1350 3200
NoConn ~ 1350 2700
NoConn ~ 1350 2600
Wire Wire Line
	1350 3600 1350 3900
Connection ~ 1350 3600
Wire Wire Line
	1350 3600 1350 3400
Wire Wire Line
	1050 3600 1350 3600
Wire Wire Line
	1550 2800 1350 2800
Wire Wire Line
	1550 2900 1550 2800
Wire Wire Line
	1550 3000 1350 3000
Wire Wire Line
	1550 3200 1550 3000
Wire Wire Line
	1450 2900 1450 3400
Wire Wire Line
	1350 2900 1450 2900
$Comp
L Connector:DB9_Female_MountingHoles J2
U 1 1 5F1FC493
P 1050 3000
F 0 "J2" H 1050 2250 50  0000 C CNN
F 1 "DB9_Female_MountingHoles" H 1000 2350 50  0000 C CNN
F 2 "Connector_Dsub:DSUB-9_Female_Horizontal_P2.77x2.84mm_EdgePinOffset4.94mm_Housed_MountingHolesOffset7.48mm" H 1050 3000 50  0001 C CNN
F 3 " ~" H 1050 3000 50  0001 C CNN
	1    1050 3000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1550 2900 2150 2900
Wire Wire Line
	1550 3200 2250 3200
Wire Wire Line
	1350 3100 2150 3100
Wire Wire Line
	1450 3400 2250 3400
Wire Wire Line
	1350 3900 1800 3900
Connection ~ 1800 3900
Wire Wire Line
	1800 3900 3050 3900
Wire Wire Line
	4150 3200 4150 3300
Wire Wire Line
	4250 3000 4250 3200
Wire Wire Line
	4350 2800 4350 3100
Wire Wire Line
	3850 2800 4350 2800
Text Notes 5900 2300 2    100  ~ 20
TTY
$Comp
L Connector_Generic:Conn_01x06 J3
U 1 1 5DF7FF45
P 5350 2400
F 0 "J3" H 5350 1900 50  0000 C CNN
F 1 "Conn_01x06" H 5350 2000 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 5350 2400 50  0001 C CNN
F 3 "~" H 5350 2400 50  0001 C CNN
	1    5350 2400
	1    0    0    1   
$EndComp
NoConn ~ 5150 2400
Text Notes 5600 2150 2    50   ~ 0
~RTS~
Text Notes 5600 2350 2    50   ~ 0
TXD
Text Notes 5700 2450 2    50   ~ 0
VCC_IO
Text Notes 5600 2550 2    50   ~ 0
~CTS
Text Notes 5600 2650 2    50   ~ 0
GND
Text Notes 5600 2250 2    50   ~ 0
RXD
Wire Wire Line
	5150 2600 5050 2600
Wire Wire Line
	5050 2600 5050 2800
Wire Wire Line
	5050 2800 5800 2800
Wire Wire Line
	5150 2500 4950 2500
Wire Wire Line
	5150 2300 4850 2300
Wire Wire Line
	5150 2200 4750 2200
Wire Wire Line
	5150 2100 4650 2100
Text Notes 9550 4850 0    50   ~ 0
Pin connections on \nFPGA mezzanine board:\n(SPI connections to 8Mb \nserial ROM.)\n\nclk50 -> A10\nspiCS -> T3\nspiMOSI -> T10\nspiMISO -> P10\nspiCLK -> R11\ndskLED -> T9\nrunLED -> R9 
Wire Wire Line
	4350 3100 4750 3100
Wire Wire Line
	4150 3300 4850 3300
Wire Wire Line
	3850 3400 4650 3400
Wire Wire Line
	4250 3200 4950 3200
Wire Wire Line
	4950 2500 4950 3200
Connection ~ 4950 3200
Wire Wire Line
	4950 3200 6000 3200
Wire Wire Line
	4850 2300 4850 3300
Connection ~ 4850 3300
Wire Wire Line
	4850 3300 6000 3300
Wire Wire Line
	4750 2200 4750 3100
Connection ~ 4750 3100
Wire Wire Line
	4750 3100 6000 3100
Wire Wire Line
	4650 2100 4650 3400
Connection ~ 4650 3400
Wire Wire Line
	4650 3400 6000 3400
NoConn ~ 1600 5600
Wire Bus Line
	9500 3200 9500 3500
Wire Bus Line
	5350 5350 5350 6350
Wire Bus Line
	7150 5350 7150 6350
Wire Bus Line
	7150 2850 7150 4850
Wire Bus Line
	9500 3800 9500 5400
Wire Bus Line
	7700 3150 7700 6350
$EndSCHEMATC
