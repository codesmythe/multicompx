EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 7 7
Title "MulticompX System Board"
Date "2020-11-29"
Rev "1.0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 "From an original design by David G. Conroy (http://www.fpgaretrocomputing.org)"
Comment4 "Copyright © 2020 Rob Gowin. Licensed under CC BY-NC-SA 4.0."
$EndDescr
Wire Wire Line
	3100 3600 3000 3600
Wire Wire Line
	3000 3600 3000 3700
Wire Wire Line
	3000 3700 3100 3700
Connection ~ 3000 3700
Wire Wire Line
	3000 3700 3000 3800
Wire Wire Line
	3000 3800 3100 3800
Connection ~ 3000 3800
Wire Wire Line
	3600 3700 4000 3700
Wire Wire Line
	4000 3700 4000 3800
Wire Wire Line
	3600 3800 4000 3800
Text Label 2850 3200 0    50   ~ 0
ED6
Text Label 2850 3300 0    50   ~ 0
ED4
Text Label 2850 3400 0    50   ~ 0
ED2
Text Label 2850 3500 0    50   ~ 0
ED0
Text Label 3800 3200 0    50   ~ 0
ED5
Text Label 3800 3300 0    50   ~ 0
ED3
Text Label 3800 3400 0    50   ~ 0
ED1
Entry Wire Line
	4000 3200 4100 3300
Entry Wire Line
	4000 3300 4100 3400
Entry Wire Line
	4000 3400 4100 3500
Entry Wire Line
	2600 3300 2700 3200
Entry Wire Line
	2600 3400 2700 3300
Entry Wire Line
	2600 3500 2700 3400
Entry Wire Line
	2600 3600 2700 3500
Text HLabel 2500 2700 0    50   BiDi ~ 0
ED[0..15]
$Comp
L power:GND #PWR0118
U 1 1 5D4FB62F
P 3700 6300
F 0 "#PWR0118" H 3700 6050 50  0001 C CNN
F 1 "GND" H 3550 6250 50  0000 C CNN
F 2 "" H 3700 6300 50  0001 C CNN
F 3 "" H 3700 6300 50  0001 C CNN
	1    3700 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0119
U 1 1 5D50402A
P 2800 3650
F 0 "#PWR0119" H 2800 3400 50  0001 C CNN
F 1 "GND" H 2800 3500 50  0000 C CNN
F 2 "" H 2800 3650 50  0001 C CNN
F 3 "" H 2800 3650 50  0001 C CNN
	1    2800 3650
	1    0    0    -1  
$EndComp
Text HLabel 2450 3850 0    50   Input ~ 0
EA[0..2]
Text HLabel 2350 5450 0    50   Input ~ 0
~ERES
Text HLabel 2350 5550 0    50   Input ~ 0
~ECS
Text HLabel 2350 5650 0    50   Input ~ 0
~EWR
Text HLabel 2350 6200 0    50   Output ~ 0
~EIRQ
Text HLabel 2350 6100 0    50   Input ~ 0
~ERD
$Comp
L Device:R R901
U 1 1 5D50DD1A
P 2700 4950
F 0 "R901" V 2600 4850 50  0000 L CNN
F 1 "10K" V 2700 4850 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 2630 4950 50  0001 C CNN
F 3 "~" H 2700 4950 50  0001 C CNN
	1    2700 4950
	1    0    0    -1  
$EndComp
Text Label 5250 2550 0    50   ~ 0
VIDEOB0
Text Label 5250 2650 0    50   ~ 0
VIDEOB1
Text Label 5250 2750 0    50   ~ 0
VIDEOR0
Text Label 5250 2850 0    50   ~ 0
VIDEOR1
Text Label 5250 2950 0    50   ~ 0
VIDEOG0
Text Label 5250 3050 0    50   ~ 0
VIDEOG1
Text Label 5250 3150 0    50   ~ 0
HSYNC
Text Label 5250 3250 0    50   ~ 0
VSYNC
$Comp
L Device:R R903
U 1 1 5DAF8B65
P 6000 2550
F 0 "R903" V 6050 2250 50  0000 L CNN
F 1 "680" V 6000 2450 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5930 2550 50  0001 C CNN
F 3 "~" H 6000 2550 50  0001 C CNN
	1    6000 2550
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R905
U 1 1 5DAFC9D2
P 6000 2750
F 0 "R905" V 6050 2450 50  0000 L CNN
F 1 "680" V 6000 2650 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5930 2750 50  0001 C CNN
F 3 "~" H 6000 2750 50  0001 C CNN
	1    6000 2750
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R907
U 1 1 5DAFE8AE
P 6000 2950
F 0 "R907" V 6050 2650 50  0000 L CNN
F 1 "680" V 6000 2850 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5930 2950 50  0001 C CNN
F 3 "~" H 6000 2950 50  0001 C CNN
	1    6000 2950
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R904
U 1 1 5DB00817
P 6000 2650
F 0 "R904" V 6050 2350 50  0000 L CNN
F 1 "470" V 6000 2550 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5930 2650 50  0001 C CNN
F 3 "~" H 6000 2650 50  0001 C CNN
	1    6000 2650
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R906
U 1 1 5DB0271D
P 6000 2850
F 0 "R906" V 6050 2550 50  0000 L CNN
F 1 "470" V 6000 2750 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5930 2850 50  0001 C CNN
F 3 "~" H 6000 2850 50  0001 C CNN
	1    6000 2850
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R908
U 1 1 5DB04605
P 6000 3050
F 0 "R908" V 6050 2750 50  0000 L CNN
F 1 "470" V 6000 2950 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5930 3050 50  0001 C CNN
F 3 "~" H 6000 3050 50  0001 C CNN
	1    6000 3050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6150 2550 6350 2550
Wire Wire Line
	6150 2650 6350 2650
Wire Wire Line
	6350 2650 6350 2550
Connection ~ 6350 2550
Wire Wire Line
	6150 2750 6350 2750
Wire Wire Line
	6150 2850 6350 2850
Wire Wire Line
	6350 2850 6350 2750
Connection ~ 6350 2750
Wire Wire Line
	6150 2950 6350 2950
Wire Wire Line
	6150 3050 6350 3050
Wire Wire Line
	6350 3050 6350 2950
Connection ~ 6350 2950
Text Label 6400 2550 0    50   ~ 0
BLUE
Text Label 6400 2750 0    50   ~ 0
RED
Text Label 6400 2950 0    50   ~ 0
GREEN
Wire Wire Line
	5200 2550 5850 2550
Wire Wire Line
	5200 2650 5850 2650
Wire Wire Line
	5200 2750 5850 2750
Wire Wire Line
	5200 2850 5850 2850
Wire Wire Line
	5200 2950 5850 2950
Wire Wire Line
	5200 3050 5850 3050
$Comp
L Connector:DB15_Female_HighDensity_MountingHoles J903
U 1 1 5DB2BF8D
P 7650 2850
F 0 "J903" H 7650 3717 50  0000 C CNN
F 1 "DB15_Female_HighDensity_MountingHoles" H 7650 3626 50  0000 C CNN
F 2 "Connector_Dsub:DSUB-15-HD_Female_Horizontal_P2.29x1.98mm_EdgePinOffset3.03mm_Housed_MountingHolesOffset4.94mm" H 6700 3250 50  0001 C CNN
F 3 " ~" H 6700 3250 50  0001 C CNN
	1    7650 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 2750 6800 2450
Wire Wire Line
	6800 2450 7350 2450
Wire Wire Line
	6350 2750 6800 2750
Wire Wire Line
	6900 2950 6900 2650
Wire Wire Line
	6900 2650 7350 2650
Wire Wire Line
	6350 2950 6900 2950
Wire Wire Line
	7000 2550 7000 2850
Wire Wire Line
	7000 2850 7350 2850
Wire Wire Line
	6350 2550 7000 2550
$Comp
L power:GND #PWR0128
U 1 1 5DB368B1
P 7650 3700
F 0 "#PWR0128" H 7650 3450 50  0001 C CNN
F 1 "GND" H 7655 3527 50  0000 C CNN
F 2 "" H 7650 3700 50  0001 C CNN
F 3 "" H 7650 3700 50  0001 C CNN
	1    7650 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 2350 7250 2350
Wire Wire Line
	7250 2350 7250 2550
Wire Wire Line
	7250 3550 7650 3550
Wire Wire Line
	7250 2550 7350 2550
Connection ~ 7250 2550
Wire Wire Line
	7250 3250 7350 3250
Wire Wire Line
	7250 2550 7250 2750
Connection ~ 7250 3250
Wire Wire Line
	7250 3250 7250 3550
Wire Wire Line
	7250 2750 7350 2750
Connection ~ 7250 2750
Wire Wire Line
	7250 2750 7250 3150
Wire Wire Line
	7650 3550 7650 3700
Connection ~ 7650 3550
Wire Wire Line
	7250 3150 7350 3150
Connection ~ 7250 3150
Wire Wire Line
	7250 3150 7250 3250
Wire Wire Line
	7050 3150 7050 4050
Wire Wire Line
	7050 4050 8050 4050
Wire Wire Line
	8050 4050 8050 2850
Wire Wire Line
	8050 2850 7950 2850
Wire Wire Line
	5200 3150 7050 3150
Wire Wire Line
	6950 3250 6950 4150
Wire Wire Line
	6950 4150 8150 4150
Wire Wire Line
	8150 4150 8150 3050
Wire Wire Line
	8150 3050 7950 3050
Wire Wire Line
	5200 3250 6950 3250
NoConn ~ 7350 3050
NoConn ~ 7950 2450
NoConn ~ 7950 2650
NoConn ~ 7950 3250
$Comp
L Connector:Mini-DIN-6 J904
U 1 1 5DB98BDC
P 7450 5200
F 0 "J904" H 7450 5567 50  0000 C CNN
F 1 "Mini-DIN-6" H 7450 5476 50  0000 C CNN
F 2 "Local:ConPS2" H 7450 5200 50  0001 C CNN
F 3 "http://service.powerdynamics.com/ec/Catalog17/Section%2011.pdf" H 7450 5200 50  0001 C CNN
	1    7450 5200
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0158
U 1 1 5DB9D590
P 6700 4350
F 0 "#PWR0158" H 6700 4200 50  0001 C CNN
F 1 "VCC" H 6717 4523 50  0000 C CNN
F 2 "" H 6700 4350 50  0001 C CNN
F 3 "" H 6700 4350 50  0001 C CNN
	1    6700 4350
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR0159
U 1 1 5DBBA04C
P 6100 4350
F 0 "#PWR0159" H 6100 4200 50  0001 C CNN
F 1 "VDD" H 6117 4523 50  0000 C CNN
F 2 "" H 6100 4350 50  0001 C CNN
F 3 "" H 6100 4350 50  0001 C CNN
	1    6100 4350
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:2N7000 Q901
U 1 1 5DDC62D6
P 6400 4700
F 0 "Q901" V 6650 4700 50  0000 C CNN
F 1 "2N7000" V 6741 4700 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 6600 4625 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N7000.pdf" H 6400 4700 50  0001 L CNN
	1    6400 4700
	0    1    1    0   
$EndComp
$Comp
L Device:R R911
U 1 1 5DDCD4C2
P 6700 4600
F 0 "R911" H 6770 4646 50  0000 L CNN
F 1 "10K" V 6700 4500 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6630 4600 50  0001 C CNN
F 3 "~" H 6700 4600 50  0001 C CNN
	1    6700 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R910
U 1 1 5DDD44EE
P 6100 4600
F 0 "R910" H 6170 4646 50  0000 L CNN
F 1 "10K" V 6100 4500 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6030 4600 50  0001 C CNN
F 3 "~" H 6100 4600 50  0001 C CNN
	1    6100 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 4450 6400 4450
Wire Wire Line
	6400 4450 6400 4500
Wire Wire Line
	6100 4350 6100 4450
Connection ~ 6100 4450
Wire Wire Line
	6700 4350 6700 4450
Wire Wire Line
	6600 4800 6700 4800
Wire Wire Line
	6700 4750 6700 4800
Connection ~ 6700 4800
Wire Wire Line
	6100 4750 6100 4800
Connection ~ 6100 4800
Wire Wire Line
	6100 4800 6200 4800
$Comp
L power:VCC #PWR0160
U 1 1 5DE26863
P 6700 5150
F 0 "#PWR0160" H 6700 5000 50  0001 C CNN
F 1 "VCC" H 6717 5323 50  0000 C CNN
F 2 "" H 6700 5150 50  0001 C CNN
F 3 "" H 6700 5150 50  0001 C CNN
	1    6700 5150
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR0161
U 1 1 5DE26869
P 6100 5150
F 0 "#PWR0161" H 6100 5000 50  0001 C CNN
F 1 "VDD" H 6117 5323 50  0000 C CNN
F 2 "" H 6100 5150 50  0001 C CNN
F 3 "" H 6100 5150 50  0001 C CNN
	1    6100 5150
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:2N7000 Q902
U 1 1 5DE2686F
P 6400 5500
F 0 "Q902" V 6650 5500 50  0000 C CNN
F 1 "2N7000" V 6741 5500 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 6600 5425 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N7000.pdf" H 6400 5500 50  0001 L CNN
	1    6400 5500
	0    1    1    0   
$EndComp
$Comp
L Device:R R913
U 1 1 5DE26875
P 6700 5400
F 0 "R913" H 6770 5446 50  0000 L CNN
F 1 "10K" V 6700 5300 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6630 5400 50  0001 C CNN
F 3 "~" H 6700 5400 50  0001 C CNN
	1    6700 5400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R912
U 1 1 5DE2687B
P 6100 5400
F 0 "R912" H 6170 5446 50  0000 L CNN
F 1 "10K" V 6100 5300 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6030 5400 50  0001 C CNN
F 3 "~" H 6100 5400 50  0001 C CNN
	1    6100 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 5250 6400 5250
Wire Wire Line
	6400 5250 6400 5300
Wire Wire Line
	6100 5150 6100 5250
Connection ~ 6100 5250
Wire Wire Line
	6700 5150 6700 5200
Wire Wire Line
	6600 5600 6700 5600
Wire Wire Line
	6700 5550 6700 5600
Connection ~ 6700 5600
Wire Wire Line
	6100 5550 6100 5600
Connection ~ 6100 5600
Wire Wire Line
	6100 5600 6200 5600
Wire Wire Line
	6700 5200 7150 5200
Connection ~ 6700 5200
Wire Wire Line
	6700 5200 6700 5250
NoConn ~ 7150 5100
NoConn ~ 7150 5300
$Comp
L power:GND #PWR0162
U 1 1 5DE3E89E
P 8150 5200
F 0 "#PWR0162" H 8150 4950 50  0001 C CNN
F 1 "GND" H 8155 5027 50  0000 C CNN
F 2 "" H 8150 5200 50  0001 C CNN
F 3 "" H 8150 5200 50  0001 C CNN
	1    8150 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 5200 8150 5200
Wire Wire Line
	7800 5600 7800 5300
Wire Wire Line
	7800 5300 7750 5300
Wire Wire Line
	6700 5600 7800 5600
Wire Wire Line
	7800 4800 7800 5100
Wire Wire Line
	7800 5100 7750 5100
Wire Wire Line
	6700 4800 7800 4800
Text Label 5750 4800 0    50   ~ 0
KCLK
Text Label 5750 5600 0    50   ~ 0
KDAT
Text HLabel 5400 4800 0    50   BiDi ~ 0
KCLK
Text HLabel 5400 5600 0    50   BiDi ~ 0
KDAT
Wire Wire Line
	5400 4800 6100 4800
Wire Wire Line
	5400 5600 6100 5600
NoConn ~ 7350 2950
Text HLabel 5200 2550 0    50   Input ~ 0
VIDEOB0
Text HLabel 5200 2650 0    50   Input ~ 0
VIDEOB1
Text HLabel 5200 2750 0    50   Input ~ 0
VIDEOR0
Text HLabel 5200 2850 0    50   Input ~ 0
VIDEOR1
Text HLabel 5200 2950 0    50   Input ~ 0
VIDEOG0
Text HLabel 5200 3050 0    50   Input ~ 0
VIDEOG1
Text HLabel 5200 3150 0    50   Input ~ 0
HSYNC
Text HLabel 5200 3250 0    50   Input ~ 0
VSYNC
Text HLabel 2500 1500 0    50   Input ~ 0
SPICLK
Wire Wire Line
	3600 5550 3800 5550
Wire Wire Line
	3600 5050 3700 5050
Text HLabel 2500 1400 0    50   Input ~ 0
SPIMOSI
Text HLabel 2500 1600 0    50   Input ~ 0
SPIMISO
$Comp
L power:VDD #PWR0138
U 1 1 5E743892
P 2450 4650
F 0 "#PWR0138" H 2450 4500 50  0001 C CNN
F 1 "VDD" H 2467 4823 50  0000 C CNN
F 2 "" H 2450 4650 50  0001 C CNN
F 3 "" H 2450 4650 50  0001 C CNN
	1    2450 4650
	1    0    0    -1  
$EndComp
Text Notes 4200 4000 0    50   ~ 0
Connectors\nfor WIZ830MJ\nmodule
Wire Wire Line
	3000 5350 3100 5350
Text HLabel 2500 1300 0    50   Input ~ 0
~SDCS
$Comp
L Connector_Generic:Conn_01x06 J?
U 1 1 5E884F53
P 4000 1500
AR Path="/5D52EB83/5E884F53" Ref="J?"  Part="1" 
AR Path="/5D9F1596/5E884F53" Ref="J905"  Part="1" 
F 0 "J905" H 4000 950 50  0000 L CNN
F 1 "Conn_01x06" H 3850 1050 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 4000 1500 50  0001 C CNN
F 3 "~" H 4000 1500 50  0001 C CNN
	1    4000 1500
	1    0    0    1   
$EndComp
Text Notes 4100 1200 0    50   ~ 0
VDD
Text Notes 4100 1300 0    50   ~ 0
~CS
Text Notes 4100 1400 0    50   ~ 0
MOSI
Text Notes 4100 1500 0    50   ~ 0
CLK
Text Notes 4100 1600 0    50   ~ 0
MISO
Text Notes 4100 1700 0    50   ~ 0
GND
$Comp
L power:VDD #PWR?
U 1 1 5E884F60
P 3000 800
AR Path="/5D52EB83/5E884F60" Ref="#PWR?"  Part="1" 
AR Path="/5D9F1596/5E884F60" Ref="#PWR0143"  Part="1" 
F 0 "#PWR0143" H 3000 650 50  0001 C CNN
F 1 "VDD" H 3017 973 50  0000 C CNN
F 2 "" H 3000 800 50  0001 C CNN
F 3 "" H 3000 800 50  0001 C CNN
	1    3000 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 1200 3800 1200
Text Notes 3600 900  0    50   ~ 0
SD Card SPI Interface.\nUse one of the many SD Card\ncard breakout boards available\non eBay, etc. On eBay, search\nfor “arduino mini microsd module”
$Comp
L Device:R R?
U 1 1 5E884F79
P 3000 1050
AR Path="/5D52EB83/5E884F79" Ref="R?"  Part="1" 
AR Path="/5D9F1596/5E884F79" Ref="R909"  Part="1" 
F 0 "R909" V 2900 950 50  0000 L CNN
F 1 "10K" V 3000 950 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 2930 1050 50  0001 C CNN
F 3 "~" H 3000 1050 50  0001 C CNN
	1    3000 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 800  3350 1200
Wire Wire Line
	3000 800  3000 900 
Connection ~ 3000 800 
Wire Wire Line
	3000 800  3350 800 
Wire Wire Line
	3000 1200 3000 1300
Connection ~ 3000 1300
Wire Wire Line
	3000 1300 3800 1300
Wire Wire Line
	2500 1300 3000 1300
Wire Wire Line
	2700 3200 3100 3200
Wire Wire Line
	2700 3300 3100 3300
Wire Wire Line
	2700 3400 3100 3400
Wire Wire Line
	2700 3500 3100 3500
Wire Wire Line
	3600 3200 4000 3200
Wire Wire Line
	3600 3300 4000 3300
Wire Wire Line
	3600 3400 4000 3400
Wire Wire Line
	3600 3500 4000 3500
Wire Bus Line
	2500 2700 2600 2700
Wire Wire Line
	2500 1600 3800 1600
Wire Wire Line
	2700 3100 3100 3100
Wire Wire Line
	2500 1400 3800 1400
Wire Wire Line
	2500 1500 3800 1500
$Comp
L Connector_Generic:Conn_02x14_Odd_Even J901
U 1 1 5E8CAB6F
P 3400 3300
F 0 "J901" H 3450 4117 50  0000 C CNN
F 1 "Conn_02x14_Odd_Even" H 3450 4026 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x14_P2.54mm_Vertical" H 3400 3300 50  0001 C CNN
F 3 "~" H 3400 3300 50  0001 C CNN
	1    3400 3300
	-1   0    0    -1  
$EndComp
$Comp
L power:VDD #PWR0147
U 1 1 5E8DA56C
P 2800 2600
F 0 "#PWR0147" H 2800 2450 50  0001 C CNN
F 1 "VDD" H 2817 2773 50  0000 C CNN
F 2 "" H 2800 2600 50  0001 C CNN
F 3 "" H 2800 2600 50  0001 C CNN
	1    2800 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 2600 2800 2700
Wire Wire Line
	2800 2700 3100 2700
$Comp
L power:GND #PWR0153
U 1 1 5E8FB014
P 3600 1800
F 0 "#PWR0153" H 3600 1550 50  0001 C CNN
F 1 "GND" H 3400 1850 50  0000 C CNN
F 2 "" H 3600 1800 50  0001 C CNN
F 3 "" H 3600 1800 50  0001 C CNN
	1    3600 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 1800 3600 1700
Wire Wire Line
	3600 1700 3800 1700
Wire Wire Line
	3600 2700 4000 2700
Wire Wire Line
	3600 2800 4000 2800
Wire Wire Line
	3600 2900 4000 2900
Wire Wire Line
	3600 3000 4000 3000
Wire Wire Line
	3600 3100 4000 3100
Text Label 3800 2700 0    50   ~ 0
ED15
Text Label 3800 2800 0    50   ~ 0
ED13
Text Label 3800 2900 0    50   ~ 0
ED11
Text Label 3800 3000 0    50   ~ 0
ED9
Text Label 3800 3100 0    50   ~ 0
ED7
Wire Wire Line
	2700 2800 3100 2800
Wire Wire Line
	2700 2900 3100 2900
Wire Wire Line
	2700 3000 3100 3000
Text Label 2850 2800 0    50   ~ 0
ED14
Text Label 2850 2900 0    50   ~ 0
ED12
Text Label 2850 3000 0    50   ~ 0
ED10
Text Label 2850 3100 0    50   ~ 0
ED8
Entry Wire Line
	2600 2900 2700 2800
Entry Wire Line
	2600 3000 2700 2900
Entry Wire Line
	2600 3100 2700 3000
Entry Wire Line
	2600 3200 2700 3100
Entry Wire Line
	4000 3000 4100 3100
Entry Wire Line
	4000 2700 4100 2800
Entry Wire Line
	4000 2800 4100 2900
Entry Wire Line
	4000 2900 4100 3000
Entry Wire Line
	4000 3100 4100 3200
Wire Wire Line
	3000 3800 3000 3900
Wire Wire Line
	2700 4000 3100 4000
Text Label 3650 4000 0    50   ~ 0
EA0
Text Label 2850 4000 0    50   ~ 0
EA1
Text Label 3650 3900 0    50   ~ 0
EA2
Entry Wire Line
	2600 4100 2700 4000
Entry Wire Line
	2600 4200 2700 4100
Wire Bus Line
	2450 3850 2600 3850
Wire Wire Line
	4000 3500 4000 3600
Wire Wire Line
	3600 3600 4000 3600
Connection ~ 4000 3600
Connection ~ 4000 3700
Wire Wire Line
	4000 3600 4000 3700
Wire Wire Line
	4000 4300 4000 3800
Wire Wire Line
	3000 4300 4000 4300
Connection ~ 4000 3800
Wire Wire Line
	3700 4100 3700 4000
Wire Wire Line
	3700 4000 3600 4000
Wire Wire Line
	2700 4100 3700 4100
Wire Wire Line
	3800 4200 3800 3900
Wire Wire Line
	3800 3900 3600 3900
Wire Wire Line
	2700 4200 3800 4200
Wire Wire Line
	3000 3900 3100 3900
Connection ~ 3000 3900
Wire Wire Line
	3000 3900 3000 4300
Text Label 2850 4100 0    50   ~ 0
EA0
Text Label 2850 4200 0    50   ~ 0
EA2
Entry Wire Line
	2600 4300 2700 4200
Wire Wire Line
	2800 3650 2800 3600
Wire Wire Line
	2800 3600 3000 3600
Connection ~ 3000 3600
$Comp
L Connector_Generic:Conn_02x14_Odd_Even J902
U 1 1 5EA7576F
P 3400 5250
F 0 "J902" H 3450 6067 50  0000 C CNN
F 1 "Conn_02x14_Odd_Even" H 3450 5976 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x14_P2.54mm_Vertical" H 3400 5250 50  0001 C CNN
F 3 "~" H 3400 5250 50  0001 C CNN
	1    3400 5250
	-1   0    0    -1  
$EndComp
NoConn ~ 3100 4750
NoConn ~ 3100 4850
NoConn ~ 3100 4950
NoConn ~ 3100 5050
NoConn ~ 3100 5150
NoConn ~ 3100 5250
Wire Wire Line
	2350 5550 2700 5550
Wire Wire Line
	2350 5650 3100 5650
Wire Wire Line
	3000 5750 3100 5750
NoConn ~ 3100 5850
NoConn ~ 3100 5950
Wire Wire Line
	3000 5350 3000 5750
Connection ~ 3000 5750
NoConn ~ 3600 4750
NoConn ~ 3600 4850
NoConn ~ 3600 4950
NoConn ~ 3600 5150
NoConn ~ 3600 5250
NoConn ~ 3600 5650
NoConn ~ 3600 5850
NoConn ~ 3600 5950
Wire Wire Line
	3800 5550 3800 6100
Wire Wire Line
	3800 6100 2350 6100
Wire Wire Line
	3900 5450 3900 6200
Wire Wire Line
	3900 6200 2350 6200
Wire Wire Line
	3600 5450 3900 5450
Wire Wire Line
	3700 6300 3000 6300
Wire Wire Line
	3000 5750 3000 6300
Wire Wire Line
	3700 5050 3700 5350
Connection ~ 3700 6300
Wire Wire Line
	3600 5750 3700 5750
Connection ~ 3700 5750
Wire Wire Line
	3700 5750 3700 6300
Wire Wire Line
	3600 5350 3700 5350
Connection ~ 3700 5350
Wire Wire Line
	3700 5350 3700 5750
Wire Wire Line
	2450 4650 2700 4650
Wire Wire Line
	2700 4800 2700 4650
Connection ~ 2700 4650
Wire Wire Line
	2700 4650 3100 4650
Wire Wire Line
	2700 5100 2700 5550
Connection ~ 2700 5550
Wire Wire Line
	2700 5550 3100 5550
Text Notes 1500 5300 0    50   ~ 0
~ERES~ is shared with ~DRES~.\nPulldown R on the IDE sheet.
Wire Wire Line
	2350 5450 3100 5450
Wire Wire Line
	3700 5050 3700 4650
Wire Wire Line
	3700 4650 3600 4650
Connection ~ 3700 5050
Wire Bus Line
	2600 3850 2600 4350
Wire Bus Line
	4100 2700 4100 3650
Wire Bus Line
	2600 2700 2600 3650
$EndSCHEMATC
