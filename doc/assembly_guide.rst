Assembly Guide
==============

Build options
-------------

Before you begin, there are a few board options to consider.

Multicomp vs PDP-10/X
^^^^^^^^^^^^^^^^^^^^^

The board was originally conceived to implement the hardware need for
Conroy's PDP-10/X.  But it can also be used simply as a Multicomp
board. This is also cheaper because you can omit four SRAM (about $20)
and the network card (about $25).

TTL Serial vs Traditional RS-232 Serial
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For the one on-board serial port, the board supports either TTL-level
UART signals, or traditional RS-232 level signals, but not both at the
same time. The six-pin TTY header can be populated either way, but if
the MAX3232 socket is populated, a USB-to-serial device plugged into
the TTL port won't work.

Register Networks vs. Discrete Resistors
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The board design has 25 33-Ohm resistors on the memory address and
control lines. This serves to slow down/dampen those signals a
bit. The resistors can either be discrete resistors, or you can use
resister network devices that look like typical DIP parts. I used
socketed resistor networks while prototyping the board so I could
change them easily, but using discrete resistors is probably cheaper,
especially if you already have them on hand. Same amount of soldering
either way. The resistor networks can either be socketed or soldered
directly to the board.

Preliminaries
-------------

Using the approach of starting with the least-tall components, we will
begin with the resistors.

Resistors, Capacitors, Diodes and Transistors
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Install these components:

* 3 resistor networks: RN401-403 or 24× 33 Ohm resistors

* 1× 33 Ohm resistor: R401 

* 12× 10K Ohm registors: R501-R504, R803, R804, R901, R909-R913

* 3× 1K Ohm resistors: R601, R801, R802

* 2× 1.8K Ohm resistors: R602, R603

* 1× 560 Ohm resistors: R805

* 3× 680 Ohm resistors: R903, R905, R907. These are 2% tolerance since
  they are in the Multicomp video circuit.

* 3× 470 Ohm resistors: R904, R906, R908. These are 2% tolerance since
  they are in the Multicomp video circuit.

Next, the capactiors, diodes and keyboard transistors:

* 8× 0.1uF bypass capacitors: C103, C104, C501-C505, C602

* 4× 0.1uF capacitors for the MAX3232 chip: C105-C108. Not
  needed if you intend to use the TTL serial option.

* 3× 10uF tantalum capacitors: C101, C102, C601. The positive end of
  the cap is the square pad on the footprint.

* 2× 1N4148 diodes: D601, D602

* 2× 2n7000 transistors: Q901, Q902

Finish the power circuit by adding

* Barrel jack

* LM1086-3.3 voltage regulator

Now, connect your 5V 2A regulated power supply to the barrel jack,
turn on the power and measure the voltage at the pads of the various
ICs. The voltage at pin 32 of the five RAM chips, pin 8 of the RTC
chip, pin 16 of the 74HCT138 chip, and the 3v3 line of the microSD
card should all be 3.3V.  Pin 1 of the 2-pin jumper JP201 should be
around 5V. For the keyboard resistors, the lower pin of the two
leftmost resistors (R910 and R912) should be 3.3V, while the lower pin
of the two rightmost ones (R911 and R913) should be 5.5V.

Serial port
^^^^^^^^^^^

If you've chosen to use a USB-to-serial device for the serial port,
install the 6-pin TTL header.

If you've chosen the traditional RS-232 serial connection, install the
DB9 connector, MAX3232 socket and MAX3232 chip (and C104-C108 if not
done previously).

As mentioned above, you can't use the TTL serial header if the MAX3232
chip is populated. Likewise, you can't use the DB9 serial port if
there's a USB-to-serial device connected to the TTL serial header.

Preparing the FPGA Board
^^^^^^^^^^^^^^^^^^^^^^^^

The QMTECH board (or clone) usually comes without any headers soldered
on. So you'll need to solder two 64-pin male header strips to the
bottom of the board. Make sure you solder to the bottom, and not to
the top.

Installing the FPGA Board
^^^^^^^^^^^^^^^^^^^^^^^^^

Unplug the power from the board. Add the 2-pin header JP201 and put a
jumper on it to close it.

Next, place the two 64-pin female pin sockets on the system board.
Then push the FPGA board gently, slightly into the pin sockets to
establish alignment. Tape the board down then solder the pin sockets,
moving quickly from pin to pin to avoid applying too much heat to the
FPGA.

Now you can fully insert the FPGA board into its connecting pin
sockets. Make sure you have the FPGA oriented correctly. Its 5V DC
connector, which we don't use, should be pointing in the opposite
direction to the 5V DC connector on the system board.

Plug power back in to the board. If you've never used the FPGA board
before, the default program of blinking the four bottom LEDs should be
running.  (Otherwise the LEDs will reflect whatever is programmed into
the FPGA.)

Simpliest Multicomp 
-------------------

At this point, enough of the board is assembled to run the simpliest
possible Multicomp system, which is a Z80 running a BASIC interpreter,
using only RAM that is internal to the FPGA and communicating via a
serial port.

Connect the serial port of the board to your computer, whichever
connection you need for your chosen serial option. Use your favorite
terminal program to initialize it and configure it for 115200 board,
eight bits, no parity, one stop bit (8N1).

The Xilinx programmer comes with a small printed circuit board, a
10-pin ribbon cable, and a 8-to-6 pin ribbon cable. We will use the
8-to-6 pin cable. Plug the small PCB into the Xilinx programmer. Plug
the 8-pin side of the ribbon cable into the small PCB. The blue wire
should plug into the VCC pin on the small PCB. Plug the 6-pin side of
the cable into J2 of the FPGA board. The blue wire should plug into
the 3v3 pin. Ensure that power is applied to the PDP10X/MulticompX
board. The STATUS light on the programmer should be green.

Download this :download:`bitstream
<../bitstreams/spartan6-sdram/test/Microcomputer_BASIC_Internal_32K_Serial.bit>`
to your computer, then use Xilinx's iMPACT software tool to download
this bitstream into the FPGA. See the last two pages of this `tutorial
<https://my.ece.utah.edu/~kalla/ECE3700/ISE_Tutorial_Nexys3_14.7_Paymon_with_W10_installation_notes_utkarsh.pdf>`_
if you've never used iMPACT before.

If all goes well, you should see::

    Z80 SBC by Grant Searle

    Memory top?

in your terminal program. Hit return here and then you will see::

    Z80 Basic Ver 4.7b
    Copyright (C) 1978 by Microsoft
    32435 Bytes free
    Ok

You can enter a simple BASIC program here and run it. A number of
games from the classic book *BASIC Computer Games* are availble at
http://vintage-basic.net/games.html. You can run many of the games
there in this BASIC interpreter (not Star Trek, unfortunately).  For
any game, click on the Source link, then Select All of the text, Copy
it, and Paste it into your terminal program. You may need to configure
your terminal program to put a delay (I use 5ms) between each
character. Once you have pasted the code, you type ``RUN`` to run the
game. Remember to use ``NEW`` between each separate game.

Multicomp with External Memory
------------------------------

Next up, we will use a Multicomp system that uses an external
SRAM for its memory.

Remove power from the board, and then remove the FPGA board from its
pin headers.

Solder in the 5x 32-pin sockets for the SRAM chips. If don't plan on
using the PDP10X functionality of the board, you can only solder in
the one socket for U505.

Populate only U505 with an SRAM chip. For the PDP10X, solder in
the other four sockets. You may optionally populate all of the 
SRAMs at this point, in order to avoid removing the FPGA board
again.

Plug the FPGA board back into the system board.  Then download this
:download:`bitsteam
<../bitstreams/spartan6-sdram/test/Microcomputer_BASIC_External_SRAM_Serial.bit>`,
which will use the U505 SRAM chip.  Power up the board and again use
the Xilinx iMPACT tool to program the FPGA with the bitstream.

After hitting return in response to the ``Memory top?`` question, you
should see::

    Z80 BASIC Ver 4.7b
    Copyright (C) 1978 by Microsoft
    57011 Bytes free
    Ok

Note the ~25K increase in 'Bytes free' from using external SRAM.

Testing other RAM chips
^^^^^^^^^^^^^^^^^^^^^^^

If you plan one using the PDP/10-X, I'd recommend doing a quick test
of the other SRAMs as well. These are not comprehensive tests; they'll
just verify basic connectivity to the SRAMs. There are four bitstream
streams available, one for each of the four other SRAMS:

.. note:: Need a table of bitstreams here.

Multicomp with VGA and PS/2
---------------------------

Solder on the VGA and PS/2 connectors. Download and program this
:download:`bitstream
<../bitstreams/spartan6-sdram/test/Microcomputer_BASIC_SRAM_VGA_PS2.bit>`,
which is the same BASIC interpreter with external memory, this time
using the VGA and PS/2 ports for display and keyboard,
respectively. (You can remove the serial port connection for now.)

You should see the ``Memory Top?`` question on your VGA monitor after
programming the FPGA, and should be able to type into the BASIC
interpreter via the PS/2 keyboard.

Final Multicomp, including SD card
----------------------------------

To complete the hardware for Multicomp, next install the socket for
U2, then install U2 itself, which is a 74HC138 chip.

Next install the SD card adapter in the lower right corner of the
board. The SD card adapter board connects "upside-down."  Solder the
adapter in place.

The bitstream for the full Multicomp is :download:`here
<../bitstreams/spartan6-sdram/MulticompZ80_VGA_PS2_SDCARD.bit>`.  The
SD card image comes from :download:`here
<https://www.retrobrewcomputers.org/lib/exe/fetch.php?media=builderpages:rhkoolstar:mc-2g-1024.zip>`.
Download that file and unzip it. In the zip is the file
``MC-2G-1024/System18.img``.  Use an image-writing tool to write that
to an SDHC card. You can get some info about image writing from the
Raspberry Pi project.  Go `here
<https://www.raspberrypi.org/documentation/installation/installing-images>`_
and skip down to "Using other tools." That page contains links to
instructions for image writing for various operatin systems.

Once you have a proper SD card image, and have programmed the FPGA,
this should appear on your VGA monitor: ::

    Press [SPACE] to activate console

Do that, then you'll see: ::

    Z80 Multicomp Multiboot Monitor v3.2
    based on a design by G. Searle

      press ? for help
    >

Type ``S2`` to boot into Z80 CP/M 2.2. For more info about this
software, see the documents in the zip file you downloaded above.

The other parts on the board, such as the IDE port, RTC clock, and
network card are not used by Multicomp, but could be if someone did
the work.


Towards the PDP10X
------------------

If you have not added all five SRAM sockets and chips, now is 
the time to do so. Then

* Solder the 8-pin socket for the RTC chip, but don't populate it yet.

* Solder in Y601, the 32.768Hz crystal for the RTC chip.

* Solder in J901 and J902, the two 2x14 female pin headers for the
  Wiznet board.

* Solder in the coin battery holder.

* Solder in the right angle IDE connector

PDP10X FPGA Programming
^^^^^^^^^^^^^^^^^^^^^^^

The Spartan-6 FPGA board has an SPI Serial ROM on-board. We will
program this ROM with a file that combines the PDP10/X FPGA bitstream
and the PDP10/X 1K-word boot ROM. Down this :download:`file
<../bitstreams/spartan6-sdram/kx.mcs>`.

Connect your choice of serial option to the board as was done for
Multicomp. Then on your PC, open a terminal programm and set the comm
parameters to 9600 baud, eight bits, no parity, 1 stop bit (8N1).

Power up the MulticompX and attach the Xilinx programmer as 
described previously so that the STATUS light is green.

Open the iMPACT tool and follow these steps:

* In the "New iMPACT Project" window, choose "create a new project
  (.ipf)" and supply a filename.

* In the "Welcome to iMPACT" window, choose "Configure devices using
  Boundary- Scan (JTAG)"

The iMPACT tool should query your programmer and display a chip icon
labelled "xc6slx16 bypass." Then:

* Click "No" to the "Do you want to continue and assign configuration
  file(s)?"  dialog box.

* Click "Cancel" on the next dialog ("Device Programming Properties").

* Above the chip icon, you will see a tiny box labelled "SPI/BPI?"
  Right click on that and select "Add SPI/BPI Flash..."

* In the "Select Attached SPI/BPI" dialog, click where it says
  "AT45DB041B" and select "M25P80" instead. Click "OK."

The tiny box above the chip icon will change to be labelled "FLASH."

* Select that so that it turns green.

* On the second window on the left, labelled "iMPACT Processes"
  double-click on "Program." Click "OK" on the next dialog box.

It will take several seconds to program the SPI ROM. After
programming, the board should reset and you should see this in your
terminal: ::

    ROM10X.85
    ?RTC ERR
    .

You can test that some memory can be zeroed by running the "Z" command
now. Then, to examine low memory, do "E/N:100 0". You should then see
a small memory dump to your terminal. You session so far would look
like this (memory addresses are in octal): ::

    ROM10X.85
    ?RTC ERR
    .Z
    .E/N:100 0
    000000 000000000000 000000000000 000000000000 000000000000
    000004 000000000000 000000000000 000000000000 000000000000
    000010 000000000000 000000000000 000000000000 000000000000
    000014 000000000000 000000000000 000000000000 000000000000
    000020 000000000000 000000000000 000000000000 000000000000
    000024 000000000000 000000000000 000000000000 000000000000
    000030 000000000000 000000000000 000000000000 000000000000
    000034 000000000000 000000000000 000000000000 000000000000
    000040 000000000000 000000000000 000000000000 000000000000
    000044 000000000000 000000000000 000000000000 000000000000
    000050 000000000000 000000000000 000000000000 000000000000
    000054 000000000000 000000000000 000000000000 000000000000
    000060 000000000000 000000000000 000000000000 000000000000
    000064 000000000000 000000000000 000000000000 000000000000
    000070 000000000000 000000000000 000000000000 000000000000
    000074 000000000000 000000000000 000000000000 000000000000
    .

To run small program, copy these lines starting with D (deposit) into
your terminal program. ::

    D 1000 200740001025
    D 1001 400000000000
    D 1002 201040000144
    D 1003 270000000001
    D 1004 366040001003
    D 1005 260740001013
    D 1006 201000000015
    D 1007 260740001021
    D 1010 201000000012
    D 1011 260740001021
    D 1012 254200001000
    D 1013 231000000012
    D 1014 261740000001
    D 1015 332000000000
    D 1016 260740001013
    D 1017 262740000000
    D 1020 271000000060
    D 1021 720340000400
    D 1022 254000001021
    D 1023 720140000000
    D 1024 263740000000
    D 1025 777766001025

This program (and all of the example code here) is from Conroy's
PDP10/X page and sums the first 100 integers in memory. First "E"
(examine) the program memory to ensure that the values made it to
memory: ::

    .E/N:26 1000
    001000 200740001025 400000000000 201040000144 270000000001
    001004 366040001003 260740001013 201000000015 260740001021
    001010 201000000012 260740001021 254200001000 231000000012
    001014 261740000001 332000000000 260740001013 262740000000
    001020 271000000060 720340000400 254000001021 720140000000
    001024 263740000000 777766001025

Then use the "S" (start) command to execute: ::

    .S 1000
    5050

At this point, the PDP10X will halt and you will need to power cycle
the board to reset. A reset button is not implemented.

The "?RTC ERR" is because we haven't installed the RTC chip, so do
that now (power off the board to insert the chip). Power the board up.

