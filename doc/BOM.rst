Parts List
==========

There are a few items that can't or shouldn't be purchased from the usual electonic part distributors (unavailable or too expensive):

* QMTECH Spartan 6 FPGA `board <https://www.ebay.com/itm/XC6SLX16-Spartan-6-Xilinx-FPGA-Development-Board-w-32Mb-Micro-SDRAM-Memory-New/133284269629>`_ or clone. 
  Approx $27 USD shipped to USA. QMTECH has two Spartan 6 boards, one with SDRAM (green) and one with DDR3 (black). 
  Be sure to get the SDRAM one. Though not currently used by Multicomp or PDP-10/X, I have ported Will SowerButt's socZ80 design, which uses SDRAM, to the board. 

* Xilinx Platform Cable USB Programmer. I use one available from
  `Waveshare <https://www.waveshare.com/product/fpga-tools/xilinx/programmers-debuggers/platform-cable-usb.htm>`_,
  but cheaper on Ebay/AliExpress. On Ebay/AliExpress, make sure you get the version 
  that has the small breakout board and extra cables.

* A USB-to-serial adapter if you want to use TTL-level serial port.  
  The board is designed to use the Adafriut `FTDI Friend <https://www.adafruit.com/product/284>`_ .
  Other breakout boards may be compatible.

* Or, a USB-to-serial cable if you want to use the RS-232 version of the serial port.

* A 5V, 2A, 5.5mm x 2.1mm regulated DC power supply (wall wart). Very common, buy at your favorite supplier.

* A 6-32 screw and nut for the LM1086 voltage regulator.

* [Multicomp] A microSD breakout board. Search Ebay/AliExpress for “arduino mini microsd module.” 

* [Multicomp] A 1 GB or greater microSD card (SDHC).

* [PDP-10/X] A 256 MB or greater compact flash card, and some way to get a disk image onto the flash card.

* [PDP-10/X] A compact flash to IDE adapter. They are available
  `here <https://www.newegg.com/syba-sd-cf-ide-di-ide-to-compact-flash/p/N82E16822998003>`_ on NewEgg.com. 
  Also available cheaper on eBay/AliExpress, but make sure the picture matches the NewEgg one.

.. error::

    The text file referenced below is net yet available. 

The rest of the BOM is listed in the table below. Here is text file that you can copy & paste
into Mouser's `BOM tool <https://www.mouser.com/Bom/CopyPaste>`_. After you add the BOM to Mouser's cart, 
be sure to carefully review your parts list and remove parts you already have or are getting elsewhere. 

.. error::

    This BOM has some mistakes. The part for J3 is wrong.

========================================== ========== ========================== ========================================================================= ======================
Designator                                 Quantity   Item                       Description                                                               Mouser Part No.       
========================================== ========== ========================== ========================================================================= ======================
X601                                                1 Battery_Cell               Coin Cell Battery Holders 20MM VERT SLIMLINE                              534-1065              
BT601                                               1 CR3202                     Coin Cell Battery 3V 30 X 3.2 MM 500mA                                    658-CR3032            
C101,C102,C601                                      3 10uF (TANT)                Tantalum Capacitors - Solid Leaded 25V 10uF 10% ESR=2.5 Ohms              581-TAP106K025CRS     
C501-C505,C602,C103-C108                           12 0.1uF                      Multilayer Ceramic Capacitors MLCC - Leaded 0.1uF 50volts 10% X7R 5mm LS  594-K104K15X7RF53H5   
D601,D602                                           2 1N4148                     Diodes - General Purpose, Power, Switching 100V Io/200mA BULK             512-1N4148            
J1                                                  1 Barrel_Jack                DC Power Connectors Power Jacks                                           490-PJ-002A           
J2                                                  1 DB9_Female_MountingHoles   D-Sub Standard Connectors DB9 Female Connector                            619-452-00005         
J201                                                1 Conn_02x20_Odd_Even        Headers & Wire Housings 40P R/A SOLDER TAIL HIGH TEMP                     517-N2540-5002RB      
J3                                                  1 Conn_01x06                 Headers & Wire Housings KK 100 Hdr Assy Bkw 02 Ckt Tin                    538-42375-1855        
J7,J8                                               2 Conn_02x32_Odd_Even        Headers & Wire Housings WR-PHD 2.54mm 64Pin Dual SocketHdr                710-61306421821       
Pin connectors for FPGA Board (male)                2 Conn_02x32_Odd_Even        Headers & Wire Housings WR-PHD 2.54mm THT 64Pin Dual Pin Headr            710-61306421121       
J901,J902                                           2 Conn_02x14_Odd_Even        Headers & Wire Housings .100" Tiger Buy Socket Strip, Square Tail         200-SSQ11401TD        
J903                                                1 DB15_Female_HighDensity    D-Sub High Density Connectors RA 15POS FEM Steel                          636-193-015-213R531   
J904                                                1 Mini-DIN-6                 DIN Connectors Mini Din Connectors                                        490-MD-60SM           
JP201                                               1 Jumper                     Headers & Wire Housings KK 100 Hdr Assy Bkw 02 Ckt Tin                    538-42375-1855        
Q901,Q902                                           2 2N7000                     MOSFET N-CHANNEL 60V 200mA                                                512-2N7000            
R401                                                1 33 ohm resistor            Carbon Film Resistors - Through Hole 33 Ohm 5%TR 1/4W                     603-CFR-25JR-5233R    
R601,R801,R802                                      3 1K ohm resistor            Carbon Film Resistors - Through Hole 1/4W 1K Ohm 5%                       603-CFR-25JT-52-1K    
R602,R603                                           2 1800 ohm resistor          Carbon Film Resistors - Through Hole 1.8K ohm 1/4W 5%                     603-CFR-25JR-521K8    
R805                                                1 560 ohm resistor           Carbon Film Resistors - Through Hole 1/4W 560 Ohm 5%                      603-CFR-25JT-52-560R  
R903,R905,R907                                      3 680 ohm resistor           Carbon Film Resistors - Through Hole 680 OHM 2% 1/4W                      660-CFS1/4CT52R681G   
R904,R906,R908                                      3 470 ohm resistor           Carbon Film Resistors - Through Hole 470 OHM 2% 1/4                       660-CFS1/4CT52R471G   
R501-R504,R803,R804,R901,R909-R913                 12 10K ohm resistor           Carbon Film Resistors - Through Hole 1/4W 10K Ohm 5%                      603-CFR-25JB-52-10K   
RN401-RN403                                         3 R_Pack08 33ohms            Resistor Networks & Arrays 16pin 33ohms Isolated Low Profile              652-4116R-1LF-33      
U1                                                  1 LT1086-3.3                 LDO Voltage Regulators 3.3V Low Dropout Pos VR 1.5A                       584-LT1086CT-3.3#PBF  
U2                                                  1 74HC138                    Encoders, Decoders, Multiplexers & Demultiplexers Line Decoder            595-SN74HCT138N       
U3                                                  1 MAX3232                    RS-232 Interface IC 3.0V to 5.5V, Low-Power, up to 1Mbps, True RS-232     700-MAX3232CPE        
U501-U505                                           5 AS6C4008-55PCN             SRAM 4M, 2.7-5.5V, 55ns 512K x 8 Asynch SRAM                              913-AS6C4008-55PCN    
U601                                                1 DS1337                     Real Time Clock IC Serial Real-Time Clock                                 700-DS1337            
X2,X3                                               2 16 Pin DIP Socket          IC & Component Sockets 16P TIN PIN TIN CONT                               575-199316            
X501-X505                                           5 32 Pin DIP Socket          IC & Component Sockets 32P TIN PIN TIN CONT                               575-1144632           
X601                                                1 8 pin DIP Socket           IC & Component Sockets 8P TIN PIN TIN CNT                                 575-144308            
Y601                                                1 32.768 kHz                 Crystals 32.768KHz10ppm 6pF                                               695-CFS-20632768EZBB  
A901                                                1 WIZ830MJ                   WIZ830MJ: Networking Modules W5300+MAG JACK                               950-WIZ830MJ    
========================================== ========== ========================== ========================================================================= ======================

