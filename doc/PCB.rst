Schematics and Build Files
==========================

My board, schematics, layout files and code changes are made available
under the `Creative Commons BY-SA-NC 4.0
license <https://creativecommons.org/licenses/by-nc-sa/4.0>`_.

============================== =======
Item                           Files
============================== =======
Schematics                    
Manufacturing Files (Gerbers) 
KiCad Files                   
============================== =======
