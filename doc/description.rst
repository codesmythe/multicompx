Description
===========

This MulticompX board an FPGA based system, originally intended to implemement
David Conroy's `PDP-10/X <http://www.fpgaretrocomputing.org/pdp10x>`_. It does that, and has since been expanded to 
implement other systems such as Grant Searle's `Multicomp <http://searle.x10host.com/Multicomp/index.html>`_. 

.. image:: images/final_board.jpg

Hardware Features
-----------------

* Supports the QMTECH Spartan-6 (with SDRAM) board (and clones). This
  board contains a Xilinx XC6SLX16 chip with about 15K logic elements
  (LE), about 72K on-board SRAM, and 108 I/O pins. The board is
  designed to be pin compatible with other QMTECH boards including
  some of the Altera ones, but I have not used one of those yet. The
  FPGA board also has a 1MB Serial ROM. This serial ROM is used by the
  PDP-10/X board. It could be used by Multicomp but currently is not.

* 2.5 MB of SRAM (5x 512KB by 8 SRAM chips)

* A serial port that can be used as a traditional RS-232 port with a
  MAX3232 chip, or used via a TTL compatible 6-pin header.

* DS1337 I2C Real Time Clock, with coin-cell battery backup (PDP-10/X
  support only)

* 40-pin IDE port (PDP-10/X support only, could be used as general GPIO
  in a Multicomp system)

* WizNet W5300 Ethernet support (PDP-10/X only)

* VGA, PS/2 (5V & 3.3V) and SD card interfaces (Multicomp only)

The board uses a standard 5V/2A wall-wart DC power supply with a
5.5mmx2.1mm connector jack.

Software Support
----------------

* As a PDP-10/X, the system runs the Incompatible Timesharing System (ITS) for
  its operating system. `ITS <https://en.wikipedia.org/wiki/Incompatible_Timesharing_System>`_
  is a historic system developed by MIT in the 1960s. Conroy ported ITS to his system.

* As a Z80 Multicomp, the system can run various versions of the CP/M and a BASIC interpret.

* As a 6809 Multicomp, the system can run CUBIX, FUZIX, and NITROS9.

* As a SocZ80 Multicomp, the system can run CP/M and FUZIX(?).

History
-------

David Conroy has
created a (customized) PDP10-on-FPGA system which he calls the
`PDP-10/X <http://www.fpgaretrocomputing.org/pdp10x>`_, and has extensively documented his work there. In
October 2018, he made an archive of the development environment of
this system available to a member of the RetroBrewComputers.org
community, who posted that archive
`here <https://www.retrobrewcomputers.org/forum/index.php?t=msg&th=333>`_.

In that thread, there were efforts to use some of Conroy's existing
boards and to use newly manufactured boards to create a working
system. However, both approaches required soldering a 208-pin 0.5mm
pitch TQFP package for the FPGA, which is challenging to say the
least.

I noticed that the same FPGA, a Xilinx Spartan 3E, was available as a
development board from WaveShare.com, so no SMT soldering
required. (They make the same board available on eBay and
AliExpress). After checking that enough pins were available on the
FPGA board, I set out to create a new system board in KiCad. I
eventually got that redesign of the system board to work, and posted
my results in that thread.

Since then, I have changed out the FPGA to use a Spartan 6 board. This
board is cheaper than the Spartan 6 board and contains a larger
FPGA. To make the system board more versatile, I've added
`Multicomp <http://searle.x10host.com/Multicomp/index.html>`_ compatible
peripherals (VGA port, PS/2 port and SD card slot).


Acknowledgements and Licenses
-----------------------------

For the PDP-10X, thanks to David Conroy for making his extensive work
available. I've simply reimplemented his System Board in KiCad and
added some Multicomp compatibiilty. But all of the Verilog to
implement the PDP-10/X is his work.

For Multicomp, thanks to Grant Searle for making the original system
available. His work was published with the following license:

“By downloading these files you must agree to the following: The
original copyright owners of ROM contents are respectfully
acknowledged. Use of the contents of any file within your own projects
is permitted freely, but any publishing of material containing whole
or part of any file distributed here, or derived from the work that I
have done here will contain an acknowledgement back to myself, Grant
Searle, and a link back to this page. Any file published or
distributed that contains all or part of any file from this page must
be made available free of charge.”
http://searle.x10host.com/Multicomp/index.html, retrieved 01/27/2020.

Thanks to Rienk Koolstra (rhkoolstar) for his useful extensions to the
Z80 Multicomp
`MC-2G-1024 <https://www.retrobrewcomputers.org/doku.php?id=builderpages:rhkoolstar:mc-2g-1024>`_
system. He also provided me a PS/2 connector footprint when I could
not find a decent one.

(If there is no license mentioned for the above works, it's because I
could not find one. Please send corrections.)
